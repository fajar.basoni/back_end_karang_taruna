﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebServiceAPI.Models
{
    public class SQLResult
    {
        public string ERRORCODE;
        public string ERRORDESC;
        public string ROWCOUNT;
        public object TAG;

        public const string NOERROR = "00";
        public const string ERROR_OCCURED = "01";
        public const string ERROR_EXCEPTION = "99";
        public const string ERROR_VALIDATION = "XXXX";
    }
}
