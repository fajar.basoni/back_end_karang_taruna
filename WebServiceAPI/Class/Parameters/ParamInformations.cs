﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebServiceAPI.Class.Parameters
{
    public class ParamInformations
    {
    }

    public class ParamGetListInformations
    {
        public string keywords { get; set; }
        public int? offset { get; set; }
        public int? limit { get; set; }
    }

    public class ParamGetDetailInformations
    {
        public string id { get; set; }
    }

    public class ParamInsertInformations
    {
        public string category_id { get; set; }
        public string information_title { get; set; }
        public string subject { get; set; }
        public string contents { get; set; }
        public string viewer { get; set; }
        public string is_deleted { get; set; }
        public string is_published { get; set; }
        public string image_path { get; set; }
        public string created_date { get; set; }
        public string created_by { get; set; }
        public string updated_date { get; set; }
        public string updated_by { get; set; }
    }

    public class ParamUpdateInformations
    {
        public string information_id { get; set; }
        public string category_id { get; set; }
        public string information_title { get; set; }
        public string subject { get; set; }
        public string contents { get; set; }
        public string viewer { get; set; }
        public string is_deleted { get; set; }
        public string is_published { get; set; }
        public string image_path { get; set; }
        public string created_date { get; set; }
        public string created_by { get; set; }
        public string updated_date { get; set; }
        public string updated_by { get; set; }
    }

    public class ParamUpdateViewerInformations
    {
        public string information_id { get; set; }
    }

    public class ParamUpdatePublishedInformations
    {
        public string information_id { get; set; }
    }

    public class ParamDeleteInformations
    {
        public string id { get; set; }
    }
}
