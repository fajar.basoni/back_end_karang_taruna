﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebServiceAPI.Class.Parameters
{
    public class ParamMemberships
    {
    }

    public class ParamGetListMemberships
    {
        public string keywords { get; set; }
        public int? offset { get; set; }
        public int? limit { get; set; }
    }

    public class ParamGetDetailMemberships
    {
        public string id { get; set; }
    }

    public class ParamInsertMemberships
    {
        public string membership_number { get; set; }
        public string name { get; set; }
        public string education_degree { get; set; }
        public string department_id { get; set; }
        public string status { get; set; }
        public string begining_periode { get; set; }
        public string ending_periode { get; set; }
        public string image_path { get; set; }
        public string is_actived { get; set; }
        public string is_deleted { get; set; }
        public string created_date { get; set; }
        public string created_by { get; set; }
        public string updated_date { get; set; }
        public string updated_by { get; set; }
    }

    public class ParamUpdateMemberships
    {
        public string membership_id { get; set; }
        public string membership_number { get; set; }
        public string name { get; set; }
        public string education_degree { get; set; }
        public string department_id { get; set; }
        public string status { get; set; }
        public string begining_periode { get; set; }
        public string ending_periode { get; set; }
        public string image_path { get; set; }
        public string is_actived { get; set; }
        public string is_deleted { get; set; }
        public string created_date { get; set; }
        public string created_by { get; set; }
        public string updated_date { get; set; }
        public string updated_by { get; set; }
    }

    public class ParamDeleteMemberships
    {
        public string id { get; set; }
    }
}
