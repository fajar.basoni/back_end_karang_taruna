﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebServiceAPI.Class.Parameters
{
    public class ParamCategory
    {
    }

    public class ParamGetListCategory
    {
        public string keywords { get; set; }
        public int? offset { get; set; }
        public int? limit { get; set; }
    }
}
