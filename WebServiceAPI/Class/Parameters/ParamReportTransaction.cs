﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebServiceAPI.Class.Parameters
{
    public class ParamReportTransaction
    {
    }

    public class ParamGetListReportTransaction
    {
        public string year { get; set; }
        public int? offset { get; set; }
        public int? limit { get; set; }
    }

    public class ParamGetDetailReportTransaction
    {
        public string id { get; set; }
    }

    public class ParamInsertReportTransaction
    {
        public string transaction_date { get; set; }
        public string account { get; set; }
        public string debit { get; set; }
        public string credit { get; set; }
        public string description { get; set; }
        public string memo { get; set; }
        public string created_date { get; set; }
        public string created_by { get; set; }
        public string updated_date { get; set; }
        public string updated_by { get; set; }
        public List<ParamInsertReportTransactionFile> files { get; set; }
    }

    public class ParamUpdateReportTransaction
    {
        public string report_transaction_id { get; set; }
        public string transaction_date { get; set; }
        public string account { get; set; }
        public string debit { get; set; }
        public string credit { get; set; }
        public string description { get; set; }
        public string memo { get; set; }
        public string created_date { get; set; }
        public string created_by { get; set; }
        public string updated_date { get; set; }
        public string updated_by { get; set; }
        public List<ParamUpdateReportTransactionFile> files { get; set; }
    }

    public class ParamDeleteReportTransaction
    {
        public string id { get; set; }
    }
}
