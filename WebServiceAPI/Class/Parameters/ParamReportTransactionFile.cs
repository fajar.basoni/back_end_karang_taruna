﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebServiceAPI.Class.Parameters
{
    public class ParamReportTransactionFile
    {
    }

    public class ParamGetDetailReportTransactionFile
    {
        public string id { get; set; }
    }

    public class ParamInsertReportTransactionFile
    {
        public string report_transaction_file_id { get; set; }
        public string report_transaction_id { get; set; }
        public string image_path { get; set; }
    }

    public class ParamUpdateReportTransactionFile
    {
        public string report_transaction_file_id { get; set; }
        public string report_transaction_id { get; set; }
        public string image_path { get; set; }
    }

    public class ParamDeleteReportTransactionFile
    {
        public string id { get; set; }
    }
}

