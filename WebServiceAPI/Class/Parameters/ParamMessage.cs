﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebServiceAPI.Class.Parameters
{
    public class ParamMessage
    {
    }
    
    public class ParamGetListMessage
    {
        public string keywords { get; set; }
        public string offset { get; set; }
        public string limit { get; set; }
    }

    public class ParamInsertMessage
    {
        public string full_name { get; set; }
        public string email { get; set; }
        public string subject { get; set; }
        public string message { get; set; }
    }

    public class ParamUpdateReadedMessage
    {
        public string message_id { get; set; }
    }
}
