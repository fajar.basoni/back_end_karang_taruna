﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebServiceAPI.Class.Parameters
{
    public class ParamAccount
    {
    }

    public class ParamGetLogin
    {
        public string username { get; set; }
        public string password { get; set; }
    }

    public class ParamGetListUsers
    {
        public string keywords { get; set; }
        public int? offset { get; set; }
        public int? limit { get; set; }
    }

    public class ParamInsertSignUp
    {
        public string username { get; set; }
        public string password { get; set; }
        public string full_name { get; set; }
        public string email { get; set; }
        public string role { get; set; }
    }
}
