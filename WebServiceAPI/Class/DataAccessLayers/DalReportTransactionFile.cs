﻿using Microsoft.ApplicationBlocks.Data;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebServiceAPI.Class.Generals;
using WebServiceAPI.Class.Parameters;
using WebServiceAPI.Class.Result;
using WebServiceAPI.Models;

namespace WebServiceAPI.Class.DataAccessLayers
{
    public class DALReportTransactionFile
    {
        #region "Configuration"
        public IConfiguration Configuration { get; }
        public DALReportTransactionFile(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public string Connect()
        {

            string strCon = Configuration["ConnectionStrings:DefaultConnection"];

            return strCon;
        }
        #endregion
        
        #region GetDetailReportTransactionFileByHeaderId
        public SQLResult GetDetailReportTransactionFileByHeaderId(ParamGetDetailReportTransactionFile value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();

            List<ResultReportTransactionFile> elements = new List<ResultReportTransactionFile>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_GET_DETAIL_REPORT_TRANSACTION_FILE_BY_HEADER_ID;

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ROWCOUNT", SqlDbType.Int, 11, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ID", value.id));
                Params.Add(new SqlParameter("@LIMIT", Configuration["AppSetting:DefaultLimitQuery"]));
                Params.Add(new SqlParameter("@OFFSET", Configuration["AppSetting:DefaultOffsetQuery"]));


                DataTable dt = SqlHelper.ExecuteDataset(Connect(),
                  CommandType.StoredProcedure, sp_name, Params.ToArray()).Tables[0];

                objResult.ERRORCODE = Params[0].Value.ToString();
                objResult.ERRORDESC = Params[1].Value.ToString();
                objResult.ROWCOUNT = Params[2].Value.ToString();

                if (objResult.ERRORCODE.Equals(SQLResult.NOERROR))
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            ResultReportTransactionFile element = new ResultReportTransactionFile();
                            element.row_id = Convert.ToString(dr["RowID"]);
                            element.report_transaction_file_id = Convert.ToString(dr["ReportTransactionFileID"]);
                            element.report_transaction_id = Convert.ToString(dr["ReportTransactionID"]);
                            element.image_path = Convert.ToString(dr["ImagePath"]);

                            elements.Add(element);
                        }
                    }

                    objResult.TAG = elements;
                }
                else
                {
                    objResult.TAG = null;
                }


                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion

        #region InsertReportTransactionFile
        public SQLResult InsertReportTransactionFile(ParamInsertReportTransactionFile value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();
            List<ResultReportTransactionFile> elements = new List<ResultReportTransactionFile>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_INSERT_REPORT_TRANSACTION_FILE;

                #region Parameter

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ReportTransactionFileID", SqlDbType.UniqueIdentifier, 255, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ReportTransactionID", value.report_transaction_id));
                Params.Add(new SqlParameter("@ImagePath", value.image_path));

                #endregion
                
                int iResult = SqlHelper.ExecuteNonQuery(Connect(),
                    CommandType.StoredProcedure,
                    sp_name,
                    Params.ToArray());

                if (iResult > 0)
                {
                    ResultReportTransactionFile element = new ResultReportTransactionFile();

                    element.report_transaction_file_id = Params[2].Value.ToString();

                    elements.Add(element);

                    objResult.ERRORCODE = Params[0].Value.ToString();
                    objResult.ERRORDESC = Params[1].Value.ToString();
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = elements;
                }
                else
                {
                    objResult.ERRORCODE = Constanta.CONST_RES_CD_DB;
                    objResult.ERRORDESC = Constanta.CONST_RES_MESSAGE_NO_ROW_AFFECTED;
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = null;
                }

                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion

        #region UpdateReportTransactionFile
        public SQLResult UpdateReportTransactionFile(ParamUpdateReportTransactionFile value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();
            List<ResultReportTransactionFile> elements = new List<ResultReportTransactionFile>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_UPDATE_REPORT_TRANSACTION_FILE;

                #region Parameter

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ReportTransactionFileID", value.report_transaction_file_id));
                Params.Add(new SqlParameter("@ReportTransactionID", value.report_transaction_id));
                Params.Add(new SqlParameter("@ImagePath", value.image_path));

                #endregion

                int iResult = SqlHelper.ExecuteNonQuery(Connect(),
                    CommandType.StoredProcedure,
                    sp_name,
                    Params.ToArray());
                
                if (iResult > 0)
                {
                    ResultReportTransactionFile element = new ResultReportTransactionFile();

                    element.report_transaction_file_id = Params[2].Value.ToString();

                    elements.Add(element);

                    objResult.ERRORCODE = Params[0].Value.ToString();
                    objResult.ERRORDESC = Params[1].Value.ToString();
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = elements;
                }
                else
                {
                    objResult.ERRORCODE = Constanta.CONST_RES_CD_DB;
                    objResult.ERRORDESC = Constanta.CONST_RES_MESSAGE_NO_ROW_AFFECTED;
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = null;
                }

                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion

        #region DeleteReportTransactionFileByHeaderId
        public SQLResult DeleteReportTransactionFileByHeaderId(ParamDeleteReportTransactionFile value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();
            List<string> elements = new List<string>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_DELETE_REPORT_TRANSACTION_FILE_BY_HEADER_ID;

                #region Parameter

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ReportTransactionID", value.id));

                #endregion

                int iResult = SqlHelper.ExecuteNonQuery(Connect(),
                    CommandType.StoredProcedure,
                    sp_name,
                    Params.ToArray());

                if (iResult > 0)
                {
                    string element = string.Empty;

                    element = Params[2].Value.ToString();

                    elements.Add(element);

                    objResult.ERRORCODE = Params[0].Value.ToString();
                    objResult.ERRORDESC = Params[1].Value.ToString();
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = elements;
                }
                else
                {
                    objResult.ERRORCODE = Constanta.CONST_RES_CD_DB;
                    objResult.ERRORDESC = Constanta.CONST_RES_MESSAGE_NO_ROW_AFFECTED;
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = null;
                }

                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion
    }
}
