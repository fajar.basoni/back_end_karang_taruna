﻿using Microsoft.ApplicationBlocks.Data;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebServiceAPI.Class.Generals;
using WebServiceAPI.Class.Parameters;
using WebServiceAPI.Class.Result;
using WebServiceAPI.Models;

namespace WebServiceAPI.Class.DataAccessLayers
{
    public class DALMemberships
    {
        #region "Configuration"
        public IConfiguration Configuration { get; }
        public DALMemberships(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public string Connect()
        {

            string strCon = Configuration["ConnectionStrings:DefaultConnection"];

            return strCon;
        }
        #endregion
        
        #region GetListMemberships
        public SQLResult GetListMemberships(ParamGetListMemberships value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();

            List<ResultMemberships> elements = new List<ResultMemberships>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_GET_LIST_MEMBERSHIPS;

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ROWCOUNT", SqlDbType.Int, 11, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@KEYWORD", value.keywords));
                Params.Add(new SqlParameter("@LIMIT", value.limit));
                Params.Add(new SqlParameter("@OFFSET", value.offset));


                DataTable dt = SqlHelper.ExecuteDataset(Connect(),
                  CommandType.StoredProcedure, sp_name, Params.ToArray()).Tables[0];

                objResult.ERRORCODE = Params[0].Value.ToString();
                objResult.ERRORDESC = Params[1].Value.ToString();
                objResult.ROWCOUNT = Params[2].Value.ToString();

                if (objResult.ERRORCODE.Equals(SQLResult.NOERROR))
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            ResultMemberships element = new ResultMemberships();
                            element.row_id = Convert.ToString(dr["RowID"]);
                            element.membership_id = Convert.ToString(dr["MembershipID"]);
                            element.membership_number = Convert.ToString(dr["MembershipNumber"]);
                            element.name = Convert.ToString(dr["Name"]);
                            element.education_degree = Convert.ToString(dr["EducationDegree"]);
                            element.department_id = Convert.ToString(dr["DepartmentID"]);
                            element.status = Convert.ToString(dr["Status"]);
                            element.begining_periode = Convert.ToString(dr["BeginingPeriode"]);
                            element.ending_periode = Convert.ToString(dr["EndingPeriode"]);
                            element.image_path = Convert.ToString(dr["ImagePath"]);
                            element.is_actived = Convert.ToString(dr["IsActived"]);
                            element.is_deleted = Convert.ToString(dr["IsDeleted"]);
                            element.created_date = Convert.ToString(dr["CreatedDate"]);
                            element.created_by = Convert.ToString(dr["CreatedBy"]);
                            element.updated_date = Convert.ToString(dr["UpdatedDate"]);
                            element.updated_by = Convert.ToString(dr["UpdatedBy"]);
                            element.department_name = Convert.ToString(dr["DepartmentName"]);

                            elements.Add(element);
                        }
                    }

                    objResult.TAG = elements;
                }
                else
                {
                    objResult.TAG = null;
                }


                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }
        #endregion

        #region GetDetailMemberships
        public SQLResult GetDetailMemberships(ParamGetDetailMemberships value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();

            List<ResultMemberships> elements = new List<ResultMemberships>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_GET_DETAIL_MEMBERSHIPS;

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ROWCOUNT", SqlDbType.Int, 11, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ID", value.id));
                Params.Add(new SqlParameter("@LIMIT", Configuration["AppSetting:DefaultLimitQuery"]));
                Params.Add(new SqlParameter("@OFFSET", Configuration["AppSetting:DefaultOffsetQuery"]));


                DataTable dt = SqlHelper.ExecuteDataset(Connect(),
                  CommandType.StoredProcedure, sp_name, Params.ToArray()).Tables[0];

                objResult.ERRORCODE = Params[0].Value.ToString();
                objResult.ERRORDESC = Params[1].Value.ToString();
                objResult.ROWCOUNT = Params[2].Value.ToString();

                if (objResult.ERRORCODE.Equals(SQLResult.NOERROR))
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            ResultMemberships element = new ResultMemberships();
                            element.row_id = Convert.ToString(dr["RowID"]);
                            element.membership_id = Convert.ToString(dr["MembershipID"]);
                            element.membership_number = Convert.ToString(dr["MembershipNumber"]);
                            element.name = Convert.ToString(dr["Name"]);
                            element.education_degree = Convert.ToString(dr["EducationDegree"]);
                            element.department_id = Convert.ToString(dr["DepartmentID"]);
                            element.status = Convert.ToString(dr["Status"]);
                            element.begining_periode = Convert.ToString(dr["BeginingPeriode"]);
                            element.ending_periode = Convert.ToString(dr["EndingPeriode"]);
                            element.image_path = Convert.ToString(dr["ImagePath"]);
                            element.is_actived = Convert.ToString(dr["IsActived"]);
                            element.is_deleted = Convert.ToString(dr["IsDeleted"]);
                            element.created_date = Convert.ToString(dr["CreatedDate"]);
                            element.created_by = Convert.ToString(dr["CreatedBy"]);
                            element.updated_date = Convert.ToString(dr["UpdatedDate"]);
                            element.updated_by = Convert.ToString(dr["UpdatedBy"]);
                            element.department_name = Convert.ToString(dr["DepartmentName"]);

                            elements.Add(element);
                        }
                    }

                    objResult.TAG = elements;
                }
                else
                {
                    objResult.TAG = null;
                }


                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion

        #region InsertMemberships
        public SQLResult InsertMemberships(ParamInsertMemberships value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();

            List<ResultMemberships> elements = new List<ResultMemberships>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_INSERT_MEMBERSHIPS;

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@MembershipID", SqlDbType.UniqueIdentifier, 255, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@MembershipNumber", value.membership_number));
                Params.Add(new SqlParameter("@Name", value.name));
                Params.Add(new SqlParameter("@EducationDegree", value.education_degree));
                Params.Add(new SqlParameter("@DepartmentID", value.department_id));
                Params.Add(new SqlParameter("@Status", value.status));
                Params.Add(new SqlParameter("@BeginingPeriode", value.begining_periode));
                Params.Add(new SqlParameter("@EndingPeriode", value.ending_periode));
                Params.Add(new SqlParameter("@ImagePath", value.image_path));
                Params.Add(new SqlParameter("@IsActived", value.is_actived));
                Params.Add(new SqlParameter("@IsDeleted", value.is_deleted));
                Params.Add(new SqlParameter("@CreatedDate", value.created_date));
                Params.Add(new SqlParameter("@CreatedBy", value.created_by));
                Params.Add(new SqlParameter("@UpdatedDate", value.updated_date));
                Params.Add(new SqlParameter("@UpdatedBy", value.updated_by));


                int iResult = SqlHelper.ExecuteNonQuery(Connect(),
                    CommandType.StoredProcedure,
                    sp_name,
                    Params.ToArray());

                if (iResult > 0)
                {
                    ResultMemberships element = new ResultMemberships();

                    element.membership_id = Params[2].Value.ToString();
                    element.updated_by = value.updated_by;
                    element.updated_date = value.updated_date;

                    elements.Add(element);

                    objResult.ERRORCODE = Params[0].Value.ToString();
                    objResult.ERRORDESC = Params[1].Value.ToString();
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = elements;
                }
                else
                {
                    objResult.ERRORCODE = Constanta.CONST_RES_CD_DB;
                    objResult.ERRORDESC = Constanta.CONST_RES_MESSAGE_NO_ROW_AFFECTED;
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = null;
                }

                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion

        #region UpdateMemberships
        public SQLResult UpdateMemberships(ParamUpdateMemberships value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();

            List<ResultMemberships> elements = new List<ResultMemberships>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_UPDATE_MEMBERSHIPS;

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@MembershipID", value.membership_id));
                Params.Add(new SqlParameter("@MembershipNumber", value.membership_number));
                Params.Add(new SqlParameter("@Name", value.name));
                Params.Add(new SqlParameter("@EducationDegree", value.education_degree));
                Params.Add(new SqlParameter("@DepartmentID", value.department_id));
                Params.Add(new SqlParameter("@Status", value.status));
                Params.Add(new SqlParameter("@BeginingPeriode", value.begining_periode));
                Params.Add(new SqlParameter("@EndingPeriode", value.ending_periode));
                Params.Add(new SqlParameter("@ImagePath", value.image_path));
                Params.Add(new SqlParameter("@IsActived", value.is_actived));
                Params.Add(new SqlParameter("@IsDeleted", value.is_deleted));
                Params.Add(new SqlParameter("@CreatedDate", value.created_date));
                Params.Add(new SqlParameter("@CreatedBy", value.created_by));
                Params.Add(new SqlParameter("@UpdatedDate", value.updated_date));
                Params.Add(new SqlParameter("@UpdatedBy", value.updated_by));


                int iResult = SqlHelper.ExecuteNonQuery(Connect(),
                    CommandType.StoredProcedure,
                    sp_name,
                    Params.ToArray());

                if (iResult > 0)
                {
                    ResultMemberships element = new ResultMemberships();

                    element.membership_id = Params[2].Value.ToString();
                    element.updated_by = value.updated_by;
                    element.updated_date = value.updated_date;

                    elements.Add(element);

                    objResult.ERRORCODE = Params[0].Value.ToString();
                    objResult.ERRORDESC = Params[1].Value.ToString();
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = elements;
                }
                else
                {
                    objResult.ERRORCODE = Constanta.CONST_RES_CD_DB;
                    objResult.ERRORDESC = Constanta.CONST_RES_MESSAGE_NO_ROW_AFFECTED;
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = null;
                }

                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion

        #region DeleteMemberships
        public SQLResult DeleteMemberships(ParamDeleteMemberships value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();

            List<string> elements = new List<string>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_DELETE_MEMBERSHIPS;

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@MembershipID", value.id));

                int iResult = SqlHelper.ExecuteNonQuery(Connect(),
                    CommandType.StoredProcedure,
                    sp_name,
                    Params.ToArray());

                if (iResult > 0)
                {
                    string element = string.Empty;

                    element = Params[2].Value.ToString();

                    elements.Add(element);

                    objResult.ERRORCODE = Params[0].Value.ToString();
                    objResult.ERRORDESC = Params[1].Value.ToString();
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = elements;
                }
                else
                {
                    objResult.ERRORCODE = Constanta.CONST_RES_CD_DB;
                    objResult.ERRORDESC = Constanta.CONST_RES_MESSAGE_NO_ROW_AFFECTED;
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = null;
                }

                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion
    }
}
