﻿using Microsoft.ApplicationBlocks.Data;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebServiceAPI.Class.Generals;
using WebServiceAPI.Class.Parameters;
using WebServiceAPI.Class.Result;
using WebServiceAPI.Models;

namespace WebServiceAPI.Class.DataAccessLayers
{
    public class DALAccount
    {
        #region "Configuration"
        public IConfiguration Configuration { get; }
        public DALAccount(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public string Connect()
        {

            string strCon = Configuration["ConnectionStrings:DefaultConnection"];

            return strCon;
        }
        #endregion

        #region GetLogin
        public SQLResult GetLogin(ParamGetLogin value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();

            List<ResultAccount> elements = new List<ResultAccount>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_GET_LOGIN;

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ROWCOUNT", SqlDbType.Int, 11, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@USERNAME", value.username));
                Params.Add(new SqlParameter("@PASSWORD", value.password));


                DataTable dt = SqlHelper.ExecuteDataset(Connect(),
                  CommandType.StoredProcedure, sp_name, Params.ToArray()).Tables[0];

                objResult.ERRORCODE = Params[0].Value.ToString();
                objResult.ERRORDESC = Params[1].Value.ToString();
                objResult.ROWCOUNT = dt.Rows.Count.ToString();

                if (objResult.ERRORCODE.Equals(SQLResult.NOERROR))
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            ResultAccount element = new ResultAccount();
                            element.user_id = Convert.ToString(dr["UserID"]);
                            element.username = Convert.ToString(dr["Username"]);
                            element.full_name = Convert.ToString(dr["FullName"]);
                            element.email = Convert.ToString(dr["Email"]);
                            element.role = Convert.ToString(dr["Role"]);

                            elements.Add(element);
                        }
                    }

                    objResult.TAG = elements;
                }
                else
                {
                    objResult.TAG = null;
                }


                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }
        #endregion

        #region GetListUsers
        public SQLResult GetListUsers(ParamGetListUsers value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();

            List<ResultAccount> elements = new List<ResultAccount>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_GET_LIST_USERS;

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ROWCOUNT", SqlDbType.Int, 11, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@KEYWORD", value.keywords));
                Params.Add(new SqlParameter("@LIMIT", value.limit));
                Params.Add(new SqlParameter("@OFFSET", value.offset));


                DataTable dt = SqlHelper.ExecuteDataset(Connect(),
                  CommandType.StoredProcedure, sp_name, Params.ToArray()).Tables[0];

                objResult.ERRORCODE = Params[0].Value.ToString();
                objResult.ERRORDESC = Params[1].Value.ToString();
                objResult.ROWCOUNT = Params[2].Value.ToString();

                if (objResult.ERRORCODE.Equals(SQLResult.NOERROR))
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            ResultAccount element = new ResultAccount();
                            element.user_id = Convert.ToString(dr["UserID"]);
                            element.username = Convert.ToString(dr["UserName"]);
                            element.full_name = Convert.ToString(dr["FullName"]);
                            element.email = Convert.ToString(dr["Email"]);
                            element.role = Convert.ToString(dr["Role"]);

                            elements.Add(element);
                        }
                    }

                    objResult.TAG = elements;
                }
                else
                {
                    objResult.TAG = null;
                }


                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion

        #region InsertSignUp
        public SQLResult InsertSignUp(ParamInsertSignUp value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();

            List<ResultAccount> elements = new List<ResultAccount>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_INSERT_SIGNUP;

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@USERID", SqlDbType.UniqueIdentifier, 255, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@USERNAME", value.username));
                Params.Add(new SqlParameter("@PASSWORD", value.password));
                Params.Add(new SqlParameter("@FULLNAME", value.full_name));
                Params.Add(new SqlParameter("@EMAIL", value.email));
                Params.Add(new SqlParameter("@ROLE", Constanta.CONST_ROLE_GENERAL));

                int iResult = SqlHelper.ExecuteNonQuery(Connect(),
                    CommandType.StoredProcedure,
                    sp_name,
                    Params.ToArray());

                if (iResult > 0)
                {
                    ResultAccount element = new ResultAccount();

                    element.user_id = Params[2].Value.ToString();
                    element.username = value.username;
                    element.full_name = value.full_name;
                    element.email = value.email;
                    element.role = Constanta.CONST_ROLE_GENERAL;

                    elements.Add(element);

                    objResult.ERRORCODE = Params[0].Value.ToString();
                    objResult.ERRORDESC = Params[1].Value.ToString();
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = elements;
                }
                else
                {
                    if (Params[0].Value.ToString() == SQLResult.ERROR_EXCEPTION)
                    {
                        objResult.ERRORCODE = Constanta.CONST_RES_CD_DB;
                        objResult.ERRORDESC = Constanta.CONST_RES_MESSAGE_NO_ROW_AFFECTED;
                        objResult.ROWCOUNT = iResult.ToString();
                        objResult.TAG = null;
                    }
                    else
                    {
                        objResult.ERRORCODE = Params[0].Value.ToString();
                        objResult.ERRORDESC = Params[1].Value.ToString();
                        objResult.ROWCOUNT = iResult.ToString();
                        objResult.TAG = null;
                    }
                }

                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }
        #endregion
    }
}
