﻿using Microsoft.ApplicationBlocks.Data;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebServiceAPI.Class.Generals;
using WebServiceAPI.Class.Parameters;
using WebServiceAPI.Class.Result;
using WebServiceAPI.Models;

namespace WebServiceAPI.Class.DataAccessLayers
{
    public class DALInformations
    {
        #region "Configuration"
        public IConfiguration Configuration { get; }
        public DALInformations(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public string Connect()
        {

            string strCon = Configuration["ConnectionStrings:DefaultConnection"];

            return strCon;
        }
        #endregion

        #region GetListInformations
        public SQLResult GetListInformations(ParamGetListInformations value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();

            List<ResultInformations> elements = new List<ResultInformations>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_GET_LIST_INFORMATIONS;

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ROWCOUNT", SqlDbType.Int, 11, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@KEYWORD", value.keywords));
                Params.Add(new SqlParameter("@LIMIT", value.limit));
                Params.Add(new SqlParameter("@OFFSET", value.offset));


                DataTable dt = SqlHelper.ExecuteDataset(Connect(),
                  CommandType.StoredProcedure, sp_name, Params.ToArray()).Tables[0];

                objResult.ERRORCODE = Params[0].Value.ToString();
                objResult.ERRORDESC = Params[1].Value.ToString();
                objResult.ROWCOUNT = Params[2].Value.ToString();

                if (objResult.ERRORCODE.Equals(SQLResult.NOERROR))
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            ResultInformations element = new ResultInformations();
                            element.row_id = Convert.ToString(dr["RowID"]);
                            element.information_id = Convert.ToString(dr["InformationID"]);
                            element.category_id = Convert.ToString(dr["CategoryID"]);
                            element.information_title = Convert.ToString(dr["InformationTitle"]);
                            element.subject = Convert.ToString(dr["Subject"]);
                            element.contents = Convert.ToString(dr["Contents"]);
                            element.viewer = Convert.ToString(dr["Viewer"]);
                            element.image_path = Convert.ToString(dr["ImagePath"]);
                            element.is_deleted = Convert.ToString(dr["IsDeleted"]);
                            element.is_published = Convert.ToString(dr["IsPublished"]);
                            element.created_date = Convert.ToString(dr["CreatedDate"]);
                            element.created_by = Convert.ToString(dr["CreatedBy"]);
                            element.updated_date = Convert.ToString(dr["UpdatedDate"]);
                            element.updated_by = Convert.ToString(dr["UpdatedBy"]);
                            element.category_name = Convert.ToString(dr["CategoryName"]);
                            element.category_desc = Convert.ToString(dr["CategoryDescription"]);

                            elements.Add(element);
                        }
                    }

                    objResult.TAG = elements;
                }
                else
                {
                    objResult.TAG = null;
                }


                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion
        
        #region GetDetailInformations
        public SQLResult GetDetailInformations(ParamGetDetailInformations value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();

            List<ResultInformations> elements = new List<ResultInformations>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_GET_DETAIL_INFORMATIONS;

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ROWCOUNT", SqlDbType.Int, 11, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ID", value.id));
                Params.Add(new SqlParameter("@LIMIT", Configuration["AppSetting:DefaultLimitQuery"]));
                Params.Add(new SqlParameter("@OFFSET", Configuration["AppSetting:DefaultOffsetQuery"]));


                DataTable dt = SqlHelper.ExecuteDataset(Connect(),
                  CommandType.StoredProcedure, sp_name, Params.ToArray()).Tables[0];

                objResult.ERRORCODE = Params[0].Value.ToString();
                objResult.ERRORDESC = Params[1].Value.ToString();
                objResult.ROWCOUNT = Params[2].Value.ToString();

                if (objResult.ERRORCODE.Equals(SQLResult.NOERROR))
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            ResultInformations element = new ResultInformations();
                            element.row_id = Convert.ToString(dr["RowID"]);
                            element.information_id = Convert.ToString(dr["InformationID"]);
                            element.category_id = Convert.ToString(dr["CategoryID"]);
                            element.information_title = Convert.ToString(dr["InformationTitle"]);
                            element.subject = Convert.ToString(dr["Subject"]);
                            element.contents = Convert.ToString(dr["Contents"]);
                            element.viewer = Convert.ToString(dr["Viewer"]);
                            element.image_path = Convert.ToString(dr["ImagePath"]);
                            element.is_deleted = Convert.ToString(dr["IsDeleted"]);
                            element.is_published = Convert.ToString(dr["IsPublished"]);
                            element.created_date = Convert.ToString(dr["CreatedDate"]);
                            element.created_by = Convert.ToString(dr["CreatedBy"]);
                            element.updated_date = Convert.ToString(dr["UpdatedDate"]);
                            element.updated_by = Convert.ToString(dr["UpdatedBy"]);
                            element.category_name = Convert.ToString(dr["CategoryName"]);
                            element.category_desc = Convert.ToString(dr["CategoryDescription"]);

                            elements.Add(element);
                        }
                    }

                    objResult.TAG = elements;
                }
                else
                {
                    objResult.TAG = null;
                }


                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion

        #region InsertInformations
        public SQLResult InsertInformations(ParamInsertInformations value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();
            List<ResultInformations> elements = new List<ResultInformations>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_INSERT_INFORMATIONS;

                #region Parameter

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@InformationID", SqlDbType.UniqueIdentifier, 255, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@CategoryID", value.category_id));
                Params.Add(new SqlParameter("@InformationTitle", value.information_title));
                Params.Add(new SqlParameter("@Subject", value.subject));
                Params.Add(new SqlParameter("@Contents", value.contents));
                Params.Add(new SqlParameter("@Viewer", value.viewer));
                Params.Add(new SqlParameter("@ImagePath", value.image_path));
                Params.Add(new SqlParameter("@IsDeleted", value.is_deleted));
                Params.Add(new SqlParameter("@IsPublished", value.is_published));
                Params.Add(new SqlParameter("@CreatedDate", value.created_date));
                Params.Add(new SqlParameter("@CreatedBy", value.created_by));
                Params.Add(new SqlParameter("@UpdatedDate", value.updated_date));
                Params.Add(new SqlParameter("@UpdatedBy", value.updated_by));

                #endregion
                
                int iResult = SqlHelper.ExecuteNonQuery(Connect(),
                    CommandType.StoredProcedure,
                    sp_name,
                    Params.ToArray());

                if (iResult > 0)
                {
                    ResultInformations element = new ResultInformations();

                    element.information_id = Params[2].Value.ToString();
                    element.updated_by = value.updated_by;
                    element.updated_date = value.updated_date;

                    elements.Add(element);

                    objResult.ERRORCODE = Params[0].Value.ToString();
                    objResult.ERRORDESC = Params[1].Value.ToString();
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = elements;
                }
                else
                {
                    objResult.ERRORCODE = Constanta.CONST_RES_CD_DB;
                    objResult.ERRORDESC = Constanta.CONST_RES_MESSAGE_NO_ROW_AFFECTED;
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = null;
                }

                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion

        #region UpdateInformations
        public SQLResult UpdateInformations(ParamUpdateInformations value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();
            List<ResultInformations> elements = new List<ResultInformations>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_UPDATE_INFORMATIONS;

                #region Parameter

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@InformationID", value.information_id));
                Params.Add(new SqlParameter("@CategoryID", value.category_id));
                Params.Add(new SqlParameter("@InformationTitle", value.information_title));
                Params.Add(new SqlParameter("@Subject", value.subject));
                Params.Add(new SqlParameter("@Contents", value.contents));
                Params.Add(new SqlParameter("@Viewer", value.viewer));
                Params.Add(new SqlParameter("@ImagePath", value.image_path));
                Params.Add(new SqlParameter("@IsDeleted", value.is_deleted));
                Params.Add(new SqlParameter("@IsPublished", value.is_published));
                Params.Add(new SqlParameter("@CreatedDate", value.created_date));
                Params.Add(new SqlParameter("@CreatedBy", value.created_by));
                Params.Add(new SqlParameter("@UpdatedDate", value.updated_date));
                Params.Add(new SqlParameter("@UpdatedBy", value.updated_by));

                #endregion

                int iResult = SqlHelper.ExecuteNonQuery(Connect(),
                    CommandType.StoredProcedure,
                    sp_name,
                    Params.ToArray());
                
                if (iResult > 0)
                {
                    ResultInformations element = new ResultInformations();

                    element.information_id = Params[2].Value.ToString();
                    element.updated_by = value.updated_by;
                    element.updated_date = value.updated_date;

                    elements.Add(element);

                    objResult.ERRORCODE = Params[0].Value.ToString();
                    objResult.ERRORDESC = Params[1].Value.ToString();
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = elements;
                }
                else
                {
                    objResult.ERRORCODE = Constanta.CONST_RES_CD_DB;
                    objResult.ERRORDESC = Constanta.CONST_RES_MESSAGE_NO_ROW_AFFECTED;
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = null;
                }

                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion

        #region UpdateViewerInformations
        public SQLResult UpdateViewerInformations(ParamUpdateViewerInformations value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();
            List<ResultInformations> elements = new List<ResultInformations>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_UPDATE_VIEWER_INFORMATIONS;

                #region Parameter

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@InformationID", value.information_id));

                #endregion

                int iResult = SqlHelper.ExecuteNonQuery(Connect(),
                    CommandType.StoredProcedure,
                    sp_name,
                    Params.ToArray());

                if (iResult > 0)
                {
                    ResultInformations element = new ResultInformations();

                    element.information_id = Params[2].Value.ToString();

                    elements.Add(element);

                    objResult.ERRORCODE = Params[0].Value.ToString();
                    objResult.ERRORDESC = Params[1].Value.ToString();
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = elements;
                }
                else
                {
                    objResult.ERRORCODE = Constanta.CONST_RES_CD_DB;
                    objResult.ERRORDESC = Constanta.CONST_RES_MESSAGE_NO_ROW_AFFECTED;
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = null;
                }

                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion

        #region UpdateViewerInformations
        public SQLResult UpdatePublishedInformations(ParamUpdatePublishedInformations value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();
            List<ResultInformations> elements = new List<ResultInformations>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_UPDATE_PUBLISHED_INFORMATIONS;

                #region Parameter

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@InformationID", value.information_id));

                #endregion

                int iResult = SqlHelper.ExecuteNonQuery(Connect(),
                    CommandType.StoredProcedure,
                    sp_name,
                    Params.ToArray());

                if (iResult > 0)
                {
                    ResultInformations element = new ResultInformations();

                    element.information_id = Params[2].Value.ToString();

                    elements.Add(element);

                    objResult.ERRORCODE = Params[0].Value.ToString();
                    objResult.ERRORDESC = Params[1].Value.ToString();
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = elements;
                }
                else
                {
                    objResult.ERRORCODE = Constanta.CONST_RES_CD_DB;
                    objResult.ERRORDESC = Constanta.CONST_RES_MESSAGE_NO_ROW_AFFECTED;
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = null;
                }

                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion

        #region DeleteInformations
        public SQLResult DeleteInformations(ParamDeleteInformations value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();
            List<string> elements = new List<string>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_DELETE_INFORMATIONS;

                #region Parameter

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@InformationID", value.id));

                #endregion

                int iResult = SqlHelper.ExecuteNonQuery(Connect(),
                    CommandType.StoredProcedure,
                    sp_name,
                    Params.ToArray());

                if (iResult > 0)
                {
                    string element = string.Empty;

                    element = Params[2].Value.ToString();

                    elements.Add(element);

                    objResult.ERRORCODE = Params[0].Value.ToString();
                    objResult.ERRORDESC = Params[1].Value.ToString();
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = elements;
                }
                else
                {
                    objResult.ERRORCODE = Constanta.CONST_RES_CD_DB;
                    objResult.ERRORDESC = Constanta.CONST_RES_MESSAGE_NO_ROW_AFFECTED;
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = null;
                }

                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion
    }
}
