﻿using Microsoft.ApplicationBlocks.Data;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebServiceAPI.Class.Generals;
using WebServiceAPI.Class.Parameters;
using WebServiceAPI.Class.Result;
using WebServiceAPI.Models;

namespace WebServiceAPI.Class.DataAccessLayers
{
    public class DALReportTransaction
    {
        #region "Configuration"
        public IConfiguration Configuration { get; }
        public DALReportTransaction(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public string Connect()
        {

            string strCon = Configuration["ConnectionStrings:DefaultConnection"];

            return strCon;
        }
        #endregion

        #region GetListReportTransaction
        public SQLResult GetListReportTransaction(ParamGetListReportTransaction value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();

            List<ResultReportTransaction> elements = new List<ResultReportTransaction>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_GET_LIST_REPORT_TRANSACTION;

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ROWCOUNT", SqlDbType.Int, 11, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@YEAR", value.year));
                Params.Add(new SqlParameter("@LIMIT", value.limit));
                Params.Add(new SqlParameter("@OFFSET", value.offset));

                DataTable dt = SqlHelper.ExecuteDataset(Connect(),
                  CommandType.StoredProcedure, sp_name, Params.ToArray()).Tables[0];

                objResult.ERRORCODE = Params[0].Value.ToString();
                objResult.ERRORDESC = Params[1].Value.ToString();
                objResult.ROWCOUNT = Params[2].Value.ToString();

                if (objResult.ERRORCODE.Equals(SQLResult.NOERROR))
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            ResultReportTransaction element = new ResultReportTransaction();
                            element.row_id = Convert.ToString(dr["RowID"]);
                            element.report_transaction_id = Convert.ToString(dr["ReportTransactionID"]);
                            element.transaction_date = Convert.ToString(dr["TransactionDate"]);
                            element.account = Convert.ToString(dr["Account"]);
                            element.debit = Convert.ToString(dr["Debit"]);
                            element.credit = Convert.ToString(dr["Credit"]);
                            element.description = Convert.ToString(dr["Description"]);
                            element.memo = Convert.ToString(dr["Memo"]);
                            element.created_date = Convert.ToString(dr["CreatedDate"]);
                            element.created_by = Convert.ToString(dr["CreatedBy"]);
                            element.updated_date = Convert.ToString(dr["UpdatedDate"]);
                            element.updated_by = Convert.ToString(dr["UpdatedBy"]);
                            element.name = Convert.ToString(dr["Name"]);

                            ParamGetDetailReportTransactionFile oParamGetDetailReportTransactionFile = new ParamGetDetailReportTransactionFile();
                            oParamGetDetailReportTransactionFile.id = element.report_transaction_id;

                            DALReportTransactionFile oDALReportTransactionFile = new DALReportTransactionFile(Configuration);
                            SQLResult objResultDataAccess = new SQLResult();
                            objResultDataAccess = oDALReportTransactionFile.GetDetailReportTransactionFileByHeaderId(oParamGetDetailReportTransactionFile);
                            element.files = (List<ResultReportTransactionFile>)objResultDataAccess.TAG;

                            elements.Add(element);
                        }
                    }

                    objResult.TAG = elements;
                }
                else
                {
                    objResult.TAG = null;
                }


                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion

        #region GetDetailReportTransaction
        public SQLResult GetDetailReportTransaction(ParamGetDetailReportTransaction value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();

            List<ResultReportTransaction> elements = new List<ResultReportTransaction>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_GET_DETAIL_REPORT_TRANSACTION;

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ROWCOUNT", SqlDbType.Int, 11, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ID", value.id));
                Params.Add(new SqlParameter("@LIMIT", Configuration["AppSetting:DefaultLimitQuery"]));
                Params.Add(new SqlParameter("@OFFSET", Configuration["AppSetting:DefaultOffsetQuery"]));


                DataTable dt = SqlHelper.ExecuteDataset(Connect(),
                  CommandType.StoredProcedure, sp_name, Params.ToArray()).Tables[0];

                objResult.ERRORCODE = Params[0].Value.ToString();
                objResult.ERRORDESC = Params[1].Value.ToString();
                objResult.ROWCOUNT = Params[2].Value.ToString();

                if (objResult.ERRORCODE.Equals(SQLResult.NOERROR))
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            ResultReportTransaction element = new ResultReportTransaction();
                            element.row_id = Convert.ToString(dr["RowID"]);
                            element.report_transaction_id = Convert.ToString(dr["ReportTransactionID"]);
                            element.transaction_date = Convert.ToString(dr["TransactionDate"]);
                            element.account = Convert.ToString(dr["Account"]);
                            element.debit = Convert.ToString(dr["Debit"]);
                            element.credit = Convert.ToString(dr["Credit"]);
                            element.description = Convert.ToString(dr["Description"]);
                            element.memo = Convert.ToString(dr["Memo"]);
                            element.created_date = Convert.ToString(dr["CreatedDate"]);
                            element.created_by = Convert.ToString(dr["CreatedBy"]);
                            element.updated_date = Convert.ToString(dr["UpdatedDate"]);
                            element.updated_by = Convert.ToString(dr["UpdatedBy"]);

                            ParamGetDetailReportTransactionFile oParamGetDetailReportTransactionFile = new ParamGetDetailReportTransactionFile();
                            oParamGetDetailReportTransactionFile.id = element.report_transaction_id;

                            DALReportTransactionFile oDALReportTransactionFile = new DALReportTransactionFile(Configuration);
                            SQLResult objResultDataAccess = new SQLResult();

                            objResultDataAccess = oDALReportTransactionFile.GetDetailReportTransactionFileByHeaderId(oParamGetDetailReportTransactionFile);
                            element.files = (List<ResultReportTransactionFile>)objResultDataAccess.TAG;

                            elements.Add(element);
                        }
                    }

                    objResult.TAG = elements;
                }
                else
                {
                    objResult.TAG = null;
                }


                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion

        #region InsertReportTransaction
        public SQLResult InsertReportTransaction(ParamInsertReportTransaction value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();
            List<ResultReportTransaction> elements = new List<ResultReportTransaction>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_INSERT_REPORT_TRANSACTION;

                #region Parameter

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ReportTransactionID", SqlDbType.UniqueIdentifier, 255, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@TransactionDate", value.transaction_date));
                Params.Add(new SqlParameter("@Account", value.account));
                Params.Add(new SqlParameter("@Debit", value.debit));
                Params.Add(new SqlParameter("@Credit", value.credit));
                Params.Add(new SqlParameter("@Description", value.description));
                Params.Add(new SqlParameter("@Memo", value.memo));
                Params.Add(new SqlParameter("@CreatedDate", value.created_date));
                Params.Add(new SqlParameter("@CreatedBy", value.created_by));
                Params.Add(new SqlParameter("@UpdatedDate", value.updated_date));
                Params.Add(new SqlParameter("@UpdatedBy", value.updated_by));

                #endregion

                int iResult = SqlHelper.ExecuteNonQuery(Connect(),
                    CommandType.StoredProcedure,
                    sp_name,
                    Params.ToArray());

                if (iResult > 0)
                {
                    ResultReportTransaction element = new ResultReportTransaction();

                    element.report_transaction_id = Params[2].Value.ToString();
                    element.updated_by = value.updated_by;
                    element.updated_date = value.updated_date;

                    foreach (ParamInsertReportTransactionFile Item in value.files)
                    {
                        ParamInsertReportTransactionFile oParamInsertReportTransactionFile = new ParamInsertReportTransactionFile();
                        oParamInsertReportTransactionFile.report_transaction_file_id = Item.report_transaction_file_id;
                        oParamInsertReportTransactionFile.report_transaction_id = element.report_transaction_id;
                        oParamInsertReportTransactionFile.image_path = Item.image_path;

                        DALReportTransactionFile oDALReportTransactionFile = new DALReportTransactionFile(Configuration);
                        oDALReportTransactionFile.InsertReportTransactionFile(oParamInsertReportTransactionFile);
                    }

                    elements.Add(element);

                    objResult.ERRORCODE = Params[0].Value.ToString();
                    objResult.ERRORDESC = Params[1].Value.ToString();
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = elements;
                }
                else
                {
                    objResult.ERRORCODE = Constanta.CONST_RES_CD_DB;
                    objResult.ERRORDESC = Constanta.CONST_RES_MESSAGE_NO_ROW_AFFECTED;
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = null;
                }

                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion

        #region UpdateReportTransaction
        public SQLResult UpdateReportTransaction(ParamUpdateReportTransaction value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();
            List<ResultReportTransaction> elements = new List<ResultReportTransaction>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_UPDATE_REPORT_TRANSACTION;

                #region Parameter

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ReportTransactionID", value.report_transaction_id));
                Params.Add(new SqlParameter("@TransactionDate", value.transaction_date));
                Params.Add(new SqlParameter("@Account", value.account));
                Params.Add(new SqlParameter("@Debit", value.debit));
                Params.Add(new SqlParameter("@Credit", value.credit));
                Params.Add(new SqlParameter("@Description", value.description));
                Params.Add(new SqlParameter("@Memo", value.memo));
                Params.Add(new SqlParameter("@CreatedDate", value.created_date));
                Params.Add(new SqlParameter("@CreatedBy", value.created_by));
                Params.Add(new SqlParameter("@UpdatedDate", value.updated_date));
                Params.Add(new SqlParameter("@UpdatedBy", value.updated_by));

                #endregion

                int iResult = SqlHelper.ExecuteNonQuery(Connect(),
                    CommandType.StoredProcedure,
                    sp_name,
                    Params.ToArray());

                if (iResult > 0)
                {
                    ResultReportTransaction element = new ResultReportTransaction();

                    element.report_transaction_id = Params[2].Value.ToString();
                    element.updated_by = value.updated_by;
                    element.updated_date = value.updated_date;

                    foreach (ParamUpdateReportTransactionFile Item in value.files)
                    {
                        DALReportTransactionFile oDALReportTransactionFile = new DALReportTransactionFile(Configuration);
                        if (!string.IsNullOrWhiteSpace(Item.report_transaction_file_id))
                        {
                            ParamUpdateReportTransactionFile oParamUpdateReportTransactionFile = new ParamUpdateReportTransactionFile();
                            oParamUpdateReportTransactionFile.report_transaction_file_id = Item.report_transaction_file_id;
                            oParamUpdateReportTransactionFile.report_transaction_id = element.report_transaction_id;
                            oParamUpdateReportTransactionFile.image_path = Item.image_path;

                            oDALReportTransactionFile.UpdateReportTransactionFile(oParamUpdateReportTransactionFile);

                        }
                        else
                        {
                            ParamInsertReportTransactionFile oParamInsertReportTransactionFile = new ParamInsertReportTransactionFile();
                            oParamInsertReportTransactionFile.report_transaction_file_id = Item.report_transaction_file_id;
                            oParamInsertReportTransactionFile.report_transaction_id = element.report_transaction_id;
                            oParamInsertReportTransactionFile.image_path = Item.image_path;

                            oDALReportTransactionFile.InsertReportTransactionFile(oParamInsertReportTransactionFile);
                        }
                    }

                    elements.Add(element);

                    objResult.ERRORCODE = Params[0].Value.ToString();
                    objResult.ERRORDESC = Params[1].Value.ToString();
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = elements;
                }
                else
                {
                    objResult.ERRORCODE = Constanta.CONST_RES_CD_DB;
                    objResult.ERRORDESC = Constanta.CONST_RES_MESSAGE_NO_ROW_AFFECTED;
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = null;
                }

                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion

        #region DeleteReportTransaction
        public SQLResult DeleteReportTransaction(ParamDeleteReportTransaction value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();
            List<string> elements = new List<string>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_DELETE_REPORT_TRANSACTION;

                #region Parameter

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ReportTransactionID", value.id));

                #endregion

                int iResult = SqlHelper.ExecuteNonQuery(Connect(),
                    CommandType.StoredProcedure,
                    sp_name,
                    Params.ToArray());

                if (iResult > 0)
                {
                    DALReportTransactionFile oDALReportTransactionFile = new DALReportTransactionFile(Configuration);
                    ParamDeleteReportTransactionFile oParamDeleteReportTransactionFile = new ParamDeleteReportTransactionFile();
                    oParamDeleteReportTransactionFile.id = value.id;
                    oDALReportTransactionFile.DeleteReportTransactionFileByHeaderId(oParamDeleteReportTransactionFile);
                    
                    string element = string.Empty;
                    element = Params[2].Value.ToString();
                    elements.Add(element);

                    objResult.ERRORCODE = Params[0].Value.ToString();
                    objResult.ERRORDESC = Params[1].Value.ToString();
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = elements;
                }
                else
                {
                    objResult.ERRORCODE = Constanta.CONST_RES_CD_DB;
                    objResult.ERRORDESC = Constanta.CONST_RES_MESSAGE_NO_ROW_AFFECTED;
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = null;
                }

                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion

        #region GetListReportYear
        public SQLResult GetListReportYear()
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();

            List<ResultReportYear> elements = new List<ResultReportYear>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_GET_LIST_REPORT_YEAR;

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ROWCOUNT", SqlDbType.Int, 11, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));


                DataTable dt = SqlHelper.ExecuteDataset(Connect(),
                  CommandType.StoredProcedure, sp_name, Params.ToArray()).Tables[0];

                objResult.ERRORCODE = Params[0].Value.ToString();
                objResult.ERRORDESC = Params[1].Value.ToString();
                objResult.ROWCOUNT = Params[2].Value.ToString();

                if (objResult.ERRORCODE.Equals(SQLResult.NOERROR))
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            ResultReportYear element = new ResultReportYear();
                            element.row_id = Convert.ToString(dr["RowID"]);
                            element.year = Convert.ToString(dr["Year"]);
                            element.total_debit = Convert.ToString(dr["TotalDebit"]);
                            element.total_credit = Convert.ToString(dr["TotalCredit"]);

                            elements.Add(element);
                        }
                    }

                    objResult.TAG = elements;
                }
                else
                {
                    objResult.TAG = null;
                }


                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion

        #region GetListReportTransaction
        public SQLResult GetListReportTransactionAccount()
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();
            List<ResultReportTransactionAccount> elements = new List<ResultReportTransactionAccount>();

            try
            {
                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ROWCOUNT", SqlDbType.Int, 11, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));

                DataTable dt = SqlHelper.ExecuteDataset(Connect(),
                  CommandType.StoredProcedure, "[dbo].[USP_GET_LIST_REPORT_TRANSACTION_ACCOUNT]", Params.ToArray()).Tables[0];

                objResult.ERRORCODE = Params[0].Value.ToString();
                objResult.ERRORDESC = Params[1].Value.ToString();
                objResult.ROWCOUNT = Params[2].Value.ToString();

                if (objResult.ERRORCODE.Equals(SQLResult.NOERROR))
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            ResultReportTransactionAccount element = new ResultReportTransactionAccount();
                            element.code = Convert.ToString(dr["Code"]);
                            element.name = Convert.ToString(dr["Name"]);
                            element.category = Convert.ToString(dr["Category"]);

                            elements.Add(element);
                        }
                    }

                    objResult.TAG = elements;
                }
                else
                {
                    objResult.TAG = null;
                }


                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }

        #endregion
    }
}
