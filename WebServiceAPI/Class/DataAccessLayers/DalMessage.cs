﻿using Microsoft.ApplicationBlocks.Data;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebServiceAPI.Class.Generals;
using WebServiceAPI.Class.Parameters;
using WebServiceAPI.Class.Result;
using WebServiceAPI.Models;

namespace WebServiceAPI.Class.DataAccessLayers
{
    public class DALMessage
    {
        #region "Configuration"
        public IConfiguration Configuration { get; }
        public DALMessage(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public string Connect()
        {

            string strCon = Configuration["ConnectionStrings:DefaultConnection"];

            return strCon;
        }
        #endregion

        #region GetListInformations
        public SQLResult GetListMessage(ParamGetListMessage value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();

            List<ResultMessage> elements = new List<ResultMessage>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_GET_LIST_MESSAGE;

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ROWCOUNT", SqlDbType.Int, 11, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@KEYWORD", value.keywords));
                Params.Add(new SqlParameter("@LIMIT", value.limit));
                Params.Add(new SqlParameter("@OFFSET", value.offset));


                DataTable dt = SqlHelper.ExecuteDataset(Connect(),
                  CommandType.StoredProcedure, sp_name, Params.ToArray()).Tables[0];

                objResult.ERRORCODE = Params[0].Value.ToString();
                objResult.ERRORDESC = Params[1].Value.ToString();
                objResult.ROWCOUNT = Params[2].Value.ToString();

                if (objResult.ERRORCODE.Equals(SQLResult.NOERROR))
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            ResultMessage element = new ResultMessage();
                            element.row_id = Convert.ToString(dr["RowID"]);
                            element.message_id = Convert.ToString(dr["MessageID"]);
                            element.full_name = Convert.ToString(dr["FullName"]);
                            element.email = Convert.ToString(dr["Email"]);
                            element.subject = Convert.ToString(dr["Subject"]);
                            element.message = Convert.ToString(dr["Message"]);
                            element.is_read = Convert.ToString(dr["IsRead"]);
                            element.created_date = Convert.ToString(dr["CreatedDate"]);

                            elements.Add(element);
                        }
                    }

                    objResult.TAG = elements;
                }
                else
                {
                    objResult.TAG = null;
                }


                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }
        #endregion

        #region InsertMessage
        public SQLResult InsertMessage(ParamInsertMessage value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();

            List<ResultMessage> elements = new List<ResultMessage>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_INSERT_MESSAGE;

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@MESSAGEID", SqlDbType.UniqueIdentifier, 255, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@FULLNAME", value.full_name));
                Params.Add(new SqlParameter("@EMAIL", value.email));
                Params.Add(new SqlParameter("@SUBJECT", value.subject));
                Params.Add(new SqlParameter("@MESSAGE", value.message));

                int iResult = SqlHelper.ExecuteNonQuery(Connect(),
                    CommandType.StoredProcedure,
                    sp_name,
                    Params.ToArray());

                if (iResult > 0)
                {
                    ResultMessage element = new ResultMessage();

                    element.message_id = Params[2].Value.ToString();
                    element.full_name = value.full_name;
                    element.email = value.email;
                    element.subject = value.subject;
                    element.message = value.message;

                    elements.Add(element);

                    objResult.ERRORCODE = Params[0].Value.ToString();
                    objResult.ERRORDESC = Params[1].Value.ToString();
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = elements;
                }
                else
                {
                    objResult.ERRORCODE = Constanta.CONST_RES_CD_DB;
                    objResult.ERRORDESC = Constanta.CONST_RES_MESSAGE_NO_ROW_AFFECTED;
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = null;
                }

                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }
        #endregion

        #region UpdateReadedMessage
        public SQLResult UpdateReadedMessage(ParamUpdateReadedMessage value)
        {
            SQLResult objResult = new SQLResult();
            List<SqlParameter> Params = new List<SqlParameter>();

            List<ResultMessage> elements = new List<ResultMessage>();

            try
            {
                string sp_name = Constanta.Query.CONST_SP_UPDATE_READED_MESSAGE;

                Params.Add(new SqlParameter("@ERRORCODE", SqlDbType.VarChar, 5, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@ERRORDESC", SqlDbType.VarChar, 500, ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Current, string.Empty));
                Params.Add(new SqlParameter("@MESSAGEID", value.message_id));

                int iResult = SqlHelper.ExecuteNonQuery(Connect(),
                    CommandType.StoredProcedure,
                    sp_name,
                    Params.ToArray());

                if (iResult > 0)
                {
                    ResultMessage element = new ResultMessage();

                    element.message_id = Params[2].Value.ToString();

                    elements.Add(element);

                    objResult.ERRORCODE = Params[0].Value.ToString();
                    objResult.ERRORDESC = Params[1].Value.ToString();
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = elements;
                }
                else
                {
                    objResult.ERRORCODE = Constanta.CONST_RES_CD_DB;
                    objResult.ERRORDESC = Constanta.CONST_RES_MESSAGE_NO_ROW_AFFECTED;
                    objResult.ROWCOUNT = iResult.ToString();
                    objResult.TAG = null;
                }

                return objResult;
            }
            catch (Exception ex)
            {
                objResult.ERRORCODE = SQLResult.ERROR_EXCEPTION;
                objResult.ERRORDESC = ex.Message.ToString();
                objResult.ROWCOUNT = "0";
                objResult.TAG = null;

                return objResult;
            }
        }
        #endregion
    }
}
