﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebServiceAPI.Class.Generals;

namespace WebServiceAPI.Class.Result
{
    public class ResultDepartments
    {
        public string row_id { get; set; }
        public string department_id { get; set; }
        public string department_name { get; set; }
        public string department_description { get; set; }
    }

    public class ResultGetListDepartments
    {
        public MetaData meta_data { get; set; }
        public List<ResultDepartments> datas { get; set; }
    }
}
