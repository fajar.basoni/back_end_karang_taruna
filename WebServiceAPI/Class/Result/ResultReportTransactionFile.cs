﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebServiceAPI.Class.Generals;

namespace WebServiceAPI.Class.Result
{
    public class ResultReportTransactionFile
    {
        public string row_id { get; set; }
        public string report_transaction_file_id { get; set; }
        public string report_transaction_id { get; set; }
        public string image_path { get; set; }
    }

    public class ResultGetListReportTransactionFile
    {
        public MetaData meta_data { get; set; }
        public List<ResultReportTransactionFile> datas { get; set; }
    }

    public class ResultGetDetailReportTransactionFile
    {
        public MetaData meta_data { get; set; }
        public List<ResultReportTransactionFile> datas { get; set; }
    }

    public class ResultInsertReportTransactionFile
    {
        public MetaData meta_data { get; set; }
        public List<ResultReportTransactionFile> datas { get; set; }
    }

    public class ResultUpdateReportTransactionFile
    {
        public MetaData meta_data { get; set; }
        public List<ResultReportTransactionFile> datas { get; set; }
    }

    public class ResultDeleteReportTransactionFile
    {
        public MetaData meta_data { get; set; }
        public List<string> datas { get; set; }
    }
}
