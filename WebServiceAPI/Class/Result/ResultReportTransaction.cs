﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebServiceAPI.Class.Generals;

namespace WebServiceAPI.Class.Result
{
    public class ResultReportTransaction
    {
        public string row_id { get; set; }
        public string report_transaction_id { get; set; }
        public string transaction_date { get; set; }
        public string account { get; set; }
        public string debit { get; set; }
        public string credit { get; set; }
        public string description { get; set; }
        public string memo { get; set; }
        public string created_date { get; set; }
        public string created_by { get; set; }
        public string updated_date { get; set; }
        public string updated_by { get; set; }
        public string name { get; set; }
        public List<ResultReportTransactionFile> files { get; set; }
    }

    public class ResultReportYear
    {
        public string row_id { get; set; }
        public string year { get; set; }
        public string total_debit { get; set; }
        public string total_credit { get; set; }
    }

    public class ResultGetListReportTransaction
    {
        public MetaData meta_data { get; set; }
        public List<ResultReportTransaction> datas { get; set; }
    }

    public class ResultGetDetailReportTransaction
    {
        public MetaData meta_data { get; set; }
        public List<ResultReportTransaction> datas { get; set; }
    }

    public class ResultInsertReportTransaction
    {
        public MetaData meta_data { get; set; }
        public List<ResultReportTransaction> datas { get; set; }
    }

    public class ResultUpdateReportTransaction
    {
        public MetaData meta_data { get; set; }
        public List<ResultReportTransaction> datas { get; set; }
    }

    public class ResultDeleteReportTransaction
    {
        public MetaData meta_data { get; set; }
        public List<string> datas { get; set; }
    }

    public class ResultGetListReportYear
    {
        public MetaData meta_data { get; set; }
        public List<ResultReportYear> datas { get; set; }
    }
}
