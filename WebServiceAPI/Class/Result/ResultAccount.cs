﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebServiceAPI.Class.Generals;

namespace WebServiceAPI.Class.Result
{
    public class ResultAccount
    {
        public string user_id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string full_name { get; set; }
        public string email { get; set; }
        public string role { get; set; }
    }

    public class ResultGetLogin
    {
        public MetaData meta_data { get; set; }
        public List<ResultAccount> datas { get; set; }
    }

    public class ResultGetListUsers
    {
        public MetaData meta_data { get; set; }
        public List<ResultAccount> datas { get; set; }
    }

    public class ResultInsertSignUp
    {
        public MetaData meta_data { get; set; }
        public List<ResultAccount> datas { get; set; }
    }
}
