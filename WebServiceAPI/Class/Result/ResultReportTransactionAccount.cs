﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebServiceAPI.Class.Generals;

namespace WebServiceAPI.Class.Result
{
    public class ResultReportTransactionAccount
    {
        public string code { get; set; }
        public string name { get; set; }
        public string category { get; set; }
    }

    public class ResultGetListReportTransactionAccount
    {
        public MetaData meta_data { get; set; }
        public List<ResultReportTransactionAccount> datas { get; set; }
    }
}
