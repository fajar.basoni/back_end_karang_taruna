﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebServiceAPI.Class.Generals;

namespace WebServiceAPI.Class.Result
{
    public class ResultInformations
    {
        public string row_id { get; set; }
        public string information_id { get; set; }
        public string category_id { get; set; }
        public string information_title { get; set; }
        public string subject { get; set; }
        public string contents { get; set; }
        public string viewer { get; set; }
        public string is_deleted { get; set; }
        public string is_published { get; set; }
        public string image_path { get; set; }
        public string created_date { get; set; }
        public string created_by { get; set; }
        public string updated_date { get; set; }
        public string updated_by { get; set; }
        public string category_name { get; set; }
        public string category_desc { get; set; }

    }

    public class ResultGetListInformations
    {
        public MetaData meta_data { get; set; }
        public List<ResultInformations> datas { get; set; }
    }

    public class ResultGetDetailInformations
    {
        public MetaData meta_data { get; set; }
        public List<ResultInformations> datas { get; set; }
    }

    public class ResultInsertInformations
    {
        public MetaData meta_data { get; set; }
        public List<ResultInformations> datas { get; set; }
    }

    public class ResultUpdateInformations
    {
        public MetaData meta_data { get; set; }
        public List<ResultInformations> datas { get; set; }
    }

    public class ResultUpdateViewerInformations
    {
        public MetaData meta_data { get; set; }
        public List<ResultInformations> datas { get; set; }
    }

    public class ResultUpdatePublishedInformations
    {
        public MetaData meta_data { get; set; }
        public List<ResultInformations> datas { get; set; }
    }

    public class ResultDeleteInformations
    {
        public MetaData meta_data { get; set; }
        public List<string> datas { get; set; }
    }
}
