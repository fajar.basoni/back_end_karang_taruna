﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebServiceAPI.Class.Generals;

namespace WebServiceAPI.Class.Result
{
    public class ResultMessage
    {
        public string row_id { get; set; }
        public string message_id { get; set; }
        public string full_name { get; set; }
        public string email { get; set; }
        public string subject { get; set; }
        public string message { get; set; }
        public string is_read { get; set; }
        public string created_date { get; set; }
    }

    public class ResultGetListMessage
    {
        public MetaData meta_data { get; set; }
        public List<ResultMessage> datas { get; set; }
    }

    public class ResultInsertMessage
    {
        public MetaData meta_data { get; set; }
        public List<ResultMessage> datas { get; set; }
    }
    public class ResultUpdateReadedMessage
    {
        public MetaData meta_data { get; set; }
        public List<ResultMessage> datas { get; set; }
    }

}
