﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebServiceAPI.Class.Generals;

namespace WebServiceAPI.Class.Result
{
    public class ResultCategory
    {
        public string row_id { get; set; }
        public string category_id { get; set; }
        public string category_name { get; set; }
        public string category_description { get; set; }
    }

    public class ResultGetListCategory
    {
        public MetaData meta_data { get; set; }
        public List<ResultCategory> datas { get; set; }
    }
}
