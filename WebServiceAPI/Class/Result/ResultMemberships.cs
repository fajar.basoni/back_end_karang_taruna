﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebServiceAPI.Class.Generals;

namespace WebServiceAPI.Class.Result
{
    public class ResultMemberships
    {
        public string row_id { get; set; }
        public string membership_id { get; set; }
        public string membership_number { get; set; }
        public string name { get; set; }
        public string education_degree { get; set; }
        public string department_id { get; set; }
        public string status { get; set; }
        public string begining_periode { get; set; }
        public string ending_periode { get; set; }
        public string image_path { get; set; }
        public string is_actived { get; set; }
        public string is_deleted { get; set; }
        public string created_date { get; set; }
        public string created_by { get; set; }
        public string updated_date { get; set; }
        public string updated_by { get; set; }
        public string department_name { get; set; }
    }

    public class ResultGetListMemberships
    {
        public MetaData meta_data { get; set; }
        public List<ResultMemberships> datas { get; set; }
    }

    public class ResultGetDetailMemberships
    {
        public MetaData meta_data { get; set; }
        public List<ResultMemberships> datas { get; set; }
    }

    public class ResultInsertMemberships
    {
        public MetaData meta_data { get; set; }
        public List<ResultMemberships> datas { get; set; }
    }

    public class ResultUpdateMemberships
    {
        public MetaData meta_data { get; set; }
        public List<ResultMemberships> datas { get; set; }
    }

    public class ResultDeleteMemberships
    {
        public MetaData meta_data { get; set; }
        public List<string> datas { get; set; }
    }
}
