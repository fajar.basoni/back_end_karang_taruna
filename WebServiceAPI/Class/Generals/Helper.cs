﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;

namespace WebServiceAPI.Class.Generals
{
    public class Helper
    {
        public bool SendEmail(string p_email_from, 
            string p_full_name, 
            string p_subject, 
            string p_message)
        {
            bool bIsSuccess = false;
            try
            {
                MailAddress fromAddress = new MailAddress(p_email_from, p_full_name);
                MailAddress toAddress = new MailAddress(Constanta.CONST_ORGANIZATION_EMAIL, Constanta.CONST_ORGANIZATION_NAME);

                SmtpClient oSmtpClient = new SmtpClient();
                oSmtpClient.Host = Constanta.CONST_SMTP_HOST;
                oSmtpClient.Port = Constanta.CONST_SMTP_PORT;
                oSmtpClient.EnableSsl = true;
                oSmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                oSmtpClient.UseDefaultCredentials = false;
                oSmtpClient.Credentials = new NetworkCredential(Constanta.CONST_SMTP_USERNAME, Constanta.CONST_SMTP_PASSWORD);

                MailMessage oMailMessage = new MailMessage();
                oMailMessage.From = fromAddress;
                oMailMessage.To.Add(toAddress);
                oMailMessage.Subject = p_subject;
                oMailMessage.Body = p_message;

                oSmtpClient.Send(oMailMessage);
                bIsSuccess = true;

                return bIsSuccess;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }

    public class AesOperation
    {
        public string Ecryption(string key, string plain_text)
        {
            byte[] iv = new byte[16];
            byte[] array;
            
            try
            {
                using (Aes aes = Aes.Create())
                {
                    aes.Key = Encoding.UTF8.GetBytes(key);
                    aes.IV = iv;

                    ICryptoTransform encrypto = aes.CreateEncryptor(aes.Key, aes.IV);

                    using (MemoryStream memory_stream = new MemoryStream())
                    {
                        using (CryptoStream crypto_stream = new CryptoStream((Stream)memory_stream, encrypto, CryptoStreamMode.Write))
                        {
                            using (StreamWriter sw = new StreamWriter((Stream)crypto_stream))
                            {
                                sw.Write(plain_text);
                            }

                            array = memory_stream.ToArray();
                        }
                    }
                }
                return Convert.ToBase64String(array);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string Decryption(string key, string cipher_text)
        {
            byte[] iv = new byte[16];
            byte[] buffer = Convert.FromBase64String(cipher_text);

            try
            {
                using (Aes aes = Aes.Create())
                {
                    aes.Key = Encoding.UTF8.GetBytes(key);
                    aes.IV = iv;

                    ICryptoTransform decrypto = aes.CreateEncryptor(aes.Key, aes.IV);

                    using (MemoryStream memory_stream = new MemoryStream(buffer))
                    {
                        using (CryptoStream crypto_stream = new CryptoStream((Stream)memory_stream, decrypto, CryptoStreamMode.Read))
                        {
                            using (StreamReader sr = new StreamReader((Stream)crypto_stream))
                            {
                                return sr.ReadToEnd();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
