﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebServiceAPI.Class.Generals
{
    public class Constanta
    {
        public const string CONST_RES_CD_SUCCESS = "200";
        public const string CONST_RES_CD_ERROR = "400";
        public const string CONST_RES_CD_CATCH = "500";
        public const string CONST_RES_CD_DB = "600";

        public const string CONST_RES_MESSAGE_SUCCESS = "Success !";
        public const string CONST_RES_MESSAGE_ERROR = "Error !";
        public const string CONST_RES_MESSAGE_FAILED = "Failed Sent!";
        public const string CONST_RES_MESSAGE_INVALIDINPUT = "Invalid Input ! ";
        public const string CONST_RES_MESSAGE_NO_ROW_AFFECTED = "No Row Affected ! ";
        public const string CONST_RES_MESSAGE_FAILED_LOGIN = "Username and Password not match !";
        public const string CONST_RES_MESSAGE_FAILED_SEND_EMAIL = "Send email failed !";

        public const string CONST_AES_KEY = "b14ca5898a4e4133bbce2ea2315a1916";
        public const string CONST_ROLE_GENERAL = "General";

        public const string CONST_ORGANIZATION_EMAIL = "kartar.bojongnangka@gmail.com";
        public const string CONST_ORGANIZATION_NAME = "Karang Taruna Desa Bojong Nangka";

        public const string CONST_SMTP_PASSWORD = "Citrapratama1";
        public const string CONST_SMTP_USERNAME = "basonifreedom@gmail.com";
        public const string CONST_SMTP_HOST = "smtp.gmail.com";
        public const string CONST_SMTP_LICENSE = "TryIt";
        public const int CONST_SMTP_PORT = 587;

        public class Query
        {
            public const string CONST_SP_GET_LIST_INFORMATIONS = "[dbo].[USP_GET_LIST_INFORMATIONS]";
            public const string CONST_SP_GET_DETAIL_INFORMATIONS = "[dbo].[USP_GET_DETAIL_INFORMATIONS]";
            public const string CONST_SP_INSERT_INFORMATIONS = "[dbo].[USP_INSERT_INFORMATIONS]";
            public const string CONST_SP_UPDATE_INFORMATIONS = "[dbo].[USP_UPDATE_INFORMATIONS]";
            public const string CONST_SP_UPDATE_VIEWER_INFORMATIONS = "[dbo].[USP_UPDATE_VIEWER_INFORMATIONS]";
            public const string CONST_SP_UPDATE_PUBLISHED_INFORMATIONS = "[dbo].[USP_UPDATE_PUBLISHED_INFORMATIONS]";
            public const string CONST_SP_DELETE_INFORMATIONS = "[dbo].[USP_DELETE_INFORMATIONS]";


            public const string CONST_SP_GET_LIST_MEMBERSHIPS = "[dbo].[USP_GET_LIST_MEMBERSHIPS]";
            public const string CONST_SP_GET_DETAIL_MEMBERSHIPS = "[dbo].[USP_GET_DETAIL_MEMBERSHIPS]";
            public const string CONST_SP_INSERT_MEMBERSHIPS = "[dbo].[USP_INSERT_MEMBERSHIPS]";
            public const string CONST_SP_UPDATE_MEMBERSHIPS = "[dbo].[USP_UPDATE_MEMBERSHIPS]";
            public const string CONST_SP_DELETE_MEMBERSHIPS = "[dbo].[USP_DELETE_MEMBERSHIPS]";

            public const string CONST_SP_GET_LIST_REPORT_TRANSACTION = "[dbo].[USP_GET_LIST_REPORT_TRANSACTION]";
            public const string CONST_SP_GET_DETAIL_REPORT_TRANSACTION = "[dbo].[USP_GET_DETAIL_REPORT_TRANSACTION]";
            public const string CONST_SP_INSERT_REPORT_TRANSACTION = "[dbo].[USP_INSERT_REPORT_TRANSACTION]";
            public const string CONST_SP_UPDATE_REPORT_TRANSACTION = "[dbo].[USP_UPDATE_REPORT_TRANSACTION]";
            public const string CONST_SP_DELETE_REPORT_TRANSACTION = "[dbo].[USP_DELETE_REPORT_TRANSACTION]";
            public const string CONST_SP_GET_LIST_REPORT_YEAR = "[dbo].[USP_GET_LIST_REPORT_YEAR]";
            
            public const string CONST_SP_GET_DETAIL_REPORT_TRANSACTION_FILE_BY_HEADER_ID = "[dbo].[USP_GET_DETAIL_REPORT_TRANSACTION_FILE_BY_HEADER_ID]";
            public const string CONST_SP_INSERT_REPORT_TRANSACTION_FILE = "[dbo].[USP_INSERT_REPORT_TRANSACTION_FILE]";
            public const string CONST_SP_UPDATE_REPORT_TRANSACTION_FILE = "[dbo].[USP_UPDATE_REPORT_TRANSACTION_FILE]";
            public const string CONST_SP_DELETE_REPORT_TRANSACTION_FILE_BY_HEADER_ID = "[dbo].[USP_DELETE_REPORT_TRANSACTION_FILE_BY_HEADER_ID]";

            public const string CONST_SP_GET_LIST_DEPARTMENTS = "[dbo].[USP_GET_LIST_DEPARTMENTS]";
            public const string CONST_SP_GET_LIST_CATEGORY = "[dbo].[USP_GET_LIST_CATEGORY]";

            public const string CONST_SP_GET_LOGIN = "[dbo].[USP_GET_LOGIN]";
            public const string CONST_SP_GET_LIST_USERS = "[dbo].[USP_GET_LIST_USERS]";
            public const string CONST_SP_INSERT_SIGNUP = "[dbo].[USP_INSERT_SIGNUP]";

            public const string CONST_SP_GET_LIST_MESSAGE = "[dbo].[USP_GET_LIST_MESSAGE]";
            public const string CONST_SP_INSERT_MESSAGE = "[dbo].[USP_INSERT_MESSAGE]";
            public const string CONST_SP_UPDATE_READED_MESSAGE = "[dbo].[USP_UPDATE_READED_MESSAGE]";
        }
    }
}
