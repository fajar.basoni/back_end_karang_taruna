﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebServiceAPI.Class.Generals
{
    public class MetaData
    {
        public string code { get; set; }
        public string message { get; set; }
        public string row_count { get; set; }
    }
}
