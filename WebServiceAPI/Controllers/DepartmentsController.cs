﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebServiceAPI.Class.DataAccessLayers;
using WebServiceAPI.Class.Generals;
using WebServiceAPI.Class.Parameters;
using WebServiceAPI.Class.Result;
using WebServiceAPI.Models;

namespace WebServiceAPI.Controllers
{
    [EnableCors("SiteCorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentsController : ControllerBase
    {
        #region Variables
        public IConfiguration Configuration { get; }
        public DepartmentsController(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        #endregion

        #region GetListDepartments
        [HttpPost]
        [Route("GetListDepartments")]
        public async Task<ResultGetListDepartments> GetListDepartments([FromBody]ParamGetListDepartments param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultGetListDepartments objResult = new ResultGetListDepartments();
            MetaData metaData = new MetaData();
            List<ResultDepartments> elements = new List<ResultDepartments>();
            bool isValid = false;
            #endregion
            
            if (isValid == false)
            {
                try
                {
                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALDepartments(Configuration).GetListDepartments(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<ResultDepartments>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion

    }
}