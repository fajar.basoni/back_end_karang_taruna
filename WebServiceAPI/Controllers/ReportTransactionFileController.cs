﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebServiceAPI.Class.DataAccessLayers;
using WebServiceAPI.Class.Generals;
using WebServiceAPI.Class.Parameters;
using WebServiceAPI.Class.Result;
using WebServiceAPI.Models;

namespace WebServiceAPI.Controllers
{
    [EnableCors("SiteCorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class ReportTransactionFileController : ControllerBase
    {
        #region Variables
        public IConfiguration Configuration { get; }
        public ReportTransactionFileController(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        #endregion
        
        #region GetDetailReportTransactionFile
        [HttpPost]
        [Route("GetDetailReportTransactionFile")]
        public async Task<ResultGetDetailReportTransactionFile> GetDetailReportTransactionFile([FromBody]ParamGetDetailReportTransactionFile param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultGetDetailReportTransactionFile objResult = new ResultGetDetailReportTransactionFile();
            MetaData metaData = new MetaData();
            List<ResultReportTransactionFile> elements = new List<ResultReportTransactionFile>();
            bool isValid = false;
            #endregion

            if (isValid == false)
            {
                try
                {
                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALReportTransactionFile(Configuration).GetDetailReportTransactionFileByHeaderId(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<ResultReportTransactionFile>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion

        #region InsertReportTransactionFile
        [HttpPost]
        [Route("InsertReportTransactionFile")]
        public async Task<ResultInsertReportTransactionFile> InsertReportTransactionFile([FromBody]ParamInsertReportTransactionFile param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultInsertReportTransactionFile objResult = new ResultInsertReportTransactionFile();
            MetaData metaData = new MetaData();
            List<ResultReportTransactionFile> elements = new List<ResultReportTransactionFile>();
            bool isValid = false;
            #endregion

            if (isValid == false)
            {
                try
                {
                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALReportTransactionFile(Configuration).InsertReportTransactionFile(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<ResultReportTransactionFile>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion

        #region UpdateReportTransactionFile
        [HttpPost]
        [Route("UpdateReportTransactionFile")]
        public async Task<ResultUpdateReportTransactionFile> UpdateReportTransactionFile([FromBody]ParamUpdateReportTransactionFile param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultUpdateReportTransactionFile objResult = new ResultUpdateReportTransactionFile();
            MetaData metaData = new MetaData();
            List<ResultReportTransactionFile> elements = new List<ResultReportTransactionFile>();
            bool isValid = false;
            #endregion

            if (isValid == false)
            {
                try
                {
                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALReportTransactionFile(Configuration).UpdateReportTransactionFile(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<ResultReportTransactionFile>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion

        #region DeleteReportTransactionFile
        [HttpPost]
        [Route("DeleteReportTransactionFile")]
        public async Task<ResultDeleteReportTransactionFile> DeleteReportTransactionFile([FromBody]ParamDeleteReportTransactionFile param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultDeleteReportTransactionFile objResult = new ResultDeleteReportTransactionFile();
            MetaData metaData = new MetaData();
            List<string> elements = new List<string>();
            bool isValid = false;
            #endregion

            if (isValid == false)
            {
                try
                {
                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALReportTransactionFile(Configuration).DeleteReportTransactionFileByHeaderId(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<string>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion

    }
}