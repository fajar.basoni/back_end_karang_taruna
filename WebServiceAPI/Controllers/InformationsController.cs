﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebServiceAPI.Class.DataAccessLayers;
using WebServiceAPI.Class.Generals;
using WebServiceAPI.Class.Parameters;
using WebServiceAPI.Class.Result;
using WebServiceAPI.Models;

namespace WebServiceAPI.Controllers
{
    [EnableCors("SiteCorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class InformationsController : ControllerBase
    {
        #region Variables
        public IConfiguration Configuration { get; }
        public InformationsController(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        #endregion

        #region GetListInformations
        [HttpPost]
        [Route("GetListInformations")]
        public async Task<ResultGetListInformations> GetListInformations([FromBody]ParamGetListInformations param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultGetListInformations objResult = new ResultGetListInformations();
            MetaData metaData = new MetaData();
            List<ResultInformations> elements = new List<ResultInformations>();
            bool isValid = false;
            #endregion
            
            if (isValid == false)
            {
                try
                {
                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALInformations(Configuration).GetListInformations(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<ResultInformations>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion
        
        #region GetDetailInformations
        [HttpPost]
        [Route("GetDetailInformations")]
        public async Task<ResultGetDetailInformations> GetDetailInformations([FromBody]ParamGetDetailInformations param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultGetDetailInformations objResult = new ResultGetDetailInformations();
            MetaData metaData = new MetaData();
            List<ResultInformations> elements = new List<ResultInformations>();
            bool isValid = false;
            #endregion

            if (!string.IsNullOrWhiteSpace(param.id))
            {
                try
                {
                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALInformations(Configuration).GetDetailInformations(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<ResultInformations>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion

        #region InsertInformations
        [HttpPost]
        [Route("InsertInformations")]
        public async Task<ResultInsertInformations> InsertInformations([FromBody]ParamInsertInformations param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultInsertInformations objResult = new ResultInsertInformations();
            MetaData metaData = new MetaData();
            List<ResultInformations> elements = new List<ResultInformations>();
            bool isValid = false;
            #endregion

            if (!string.IsNullOrWhiteSpace(param.category_id)
                && !string.IsNullOrWhiteSpace(param.information_title)
                && !string.IsNullOrWhiteSpace(param.subject)
                && !string.IsNullOrWhiteSpace(param.contents)
                && !string.IsNullOrWhiteSpace(param.viewer)
                && !string.IsNullOrWhiteSpace(param.is_deleted)
                && !string.IsNullOrWhiteSpace(param.is_published)
                && !string.IsNullOrWhiteSpace(param.image_path)
                && !string.IsNullOrWhiteSpace(param.created_date)
                && !string.IsNullOrWhiteSpace(param.created_by)
                && !string.IsNullOrWhiteSpace(param.updated_date)
                && !string.IsNullOrWhiteSpace(param.updated_by))
            {
                isValid = true;
            }

            if(isValid)
            {
                try
                {
                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALInformations(Configuration).InsertInformations(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<ResultInformations>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion

        #region UpdateInformations
        [HttpPost]
        [Route("UpdateInformations")]
        public async Task<ResultUpdateInformations> UpdateInformations([FromBody]ParamUpdateInformations param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultUpdateInformations objResult = new ResultUpdateInformations();
            MetaData metaData = new MetaData();
            List<ResultInformations> elements = new List<ResultInformations>();
            bool isValid = false;
            #endregion

            if(!string.IsNullOrWhiteSpace(param.information_id)
                && !string.IsNullOrWhiteSpace(param.category_id)
                && !string.IsNullOrWhiteSpace(param.information_title)
                && !string.IsNullOrWhiteSpace(param.subject)
                && !string.IsNullOrWhiteSpace(param.contents)
                && !string.IsNullOrWhiteSpace(param.viewer)
                && !string.IsNullOrWhiteSpace(param.is_deleted)
                && !string.IsNullOrWhiteSpace(param.is_published)
                && !string.IsNullOrWhiteSpace(param.image_path)
                && !string.IsNullOrWhiteSpace(param.created_date)
                && !string.IsNullOrWhiteSpace(param.created_by)
                && !string.IsNullOrWhiteSpace(param.updated_date)
                && !string.IsNullOrWhiteSpace(param.updated_by))
            {
                isValid = true;
            }

            if (isValid)
            {
                try
                {
                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALInformations(Configuration).UpdateInformations(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<ResultInformations>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion

        #region UpdateViewerInformations
        [HttpPost]
        [Route("UpdateViewerInformations")]
        public async Task<ResultUpdateViewerInformations> UpdateViewerInformations([FromBody] ParamUpdateViewerInformations param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultUpdateViewerInformations objResult = new ResultUpdateViewerInformations();
            MetaData metaData = new MetaData();
            List<ResultInformations> elements = new List<ResultInformations>();
            bool isValid = false;
            #endregion

            if (!string.IsNullOrWhiteSpace(param.information_id))
            {
                isValid = true;
            }

            if (isValid)
            {
                try
                {
                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALInformations(Configuration).UpdateViewerInformations(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<ResultInformations>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion

        #region UpdateViewerInformations
        [HttpPost]
        [Route("UpdatePublishedInformations")]
        public async Task<ResultUpdatePublishedInformations> UpdatePublishedInformations([FromBody] ParamUpdatePublishedInformations param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultUpdatePublishedInformations objResult = new ResultUpdatePublishedInformations();
            MetaData metaData = new MetaData();
            List<ResultInformations> elements = new List<ResultInformations>();
            bool isValid = false;
            #endregion

            if (!string.IsNullOrWhiteSpace(param.information_id))
            {
                isValid = true;
            }

            if (isValid)
            {
                try
                {
                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALInformations(Configuration).UpdatePublishedInformations(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<ResultInformations>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion

        #region UpdateInformations
        [HttpPost]
        [Route("DeleteInformations")]
        public async Task<ResultDeleteInformations> DeleteInformations([FromBody]ParamDeleteInformations param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultDeleteInformations objResult = new ResultDeleteInformations();
            MetaData metaData = new MetaData();
            List<string> elements = new List<string>();
            bool isValid = false;
            #endregion

            if (isValid == false)
            {
                try
                {
                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALInformations(Configuration).DeleteInformations(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<string>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion

    }
}