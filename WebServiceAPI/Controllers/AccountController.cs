﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebServiceAPI.Class.DataAccessLayers;
using WebServiceAPI.Class.Generals;
using WebServiceAPI.Class.Parameters;
using WebServiceAPI.Class.Result;
using WebServiceAPI.Models;

namespace WebServiceAPI.Controllers
{
    [EnableCors("SiteCorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        #region Variables
        public IConfiguration Configuration { get; }
        public AccountController(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        #endregion

        #region GetLogin
        [HttpPost]
        [Route("GetLogin")]
        public async Task<ResultGetLogin> GetLogin([FromBody]ParamGetLogin param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultGetLogin objResult = new ResultGetLogin();
            MetaData metaData = new MetaData();
            List<ResultAccount> elements = new List<ResultAccount>();
            bool isValid = false;
            #endregion

            //1. Validation parameter
            if(!string.IsNullOrWhiteSpace(param.username)
                && !string.IsNullOrWhiteSpace(param.password))
            {
                isValid = true;
            }

            if (isValid)
            {
                try
                {
                    //Get Encrypt Password
                    AesOperation aes_operation = new AesOperation();
                    param.password = aes_operation.Ecryption(Constanta.CONST_AES_KEY, param.password);

                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALAccount(Configuration).GetLogin(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        elements = (List<ResultAccount>)objResultDataAccess.TAG;

                        if (elements.Count > 0)
                        {
                            //#. success {code=200}!
                            metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                            metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. success {code=200}!
                            metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                            metaData.message = Constanta.CONST_RES_MESSAGE_FAILED_LOGIN;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion

        #region InsertSignUp
        [HttpPost]
        [Route("InsertSignUp")]
        public async Task<ResultInsertSignUp> InsertSignUp([FromBody] ParamInsertSignUp param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultInsertSignUp objResult = new ResultInsertSignUp();
            MetaData metaData = new MetaData();
            List<ResultAccount> elements = new List<ResultAccount>();
            bool isValid = false;
            #endregion

            //1. Validation parameter
            if (!string.IsNullOrWhiteSpace(param.username)
                && !string.IsNullOrWhiteSpace(param.password)
                && !string.IsNullOrWhiteSpace(param.full_name)
                && !string.IsNullOrWhiteSpace(param.email))
            {
                isValid = true;
            }

            if (isValid)
            {
                try
                {
                    //Get Encrypt Password
                    AesOperation aes_operation = new AesOperation();
                    param.password = aes_operation.Ecryption(Constanta.CONST_AES_KEY, param.password);

                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALAccount(Configuration).InsertSignUp(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<ResultAccount>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion

        #region GetListUsers
        [HttpPost]
        [Route("GetListUsers")]
        public async Task<ResultGetListUsers> GetListUsers([FromBody]ParamGetListUsers param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultGetListUsers objResult = new ResultGetListUsers();
            MetaData metaData = new MetaData();
            List<ResultAccount> elements = new List<ResultAccount>();
            bool isValid = false;
            #endregion

            if (isValid == false)
            {
                try
                {
                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALAccount(Configuration).GetListUsers(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<ResultAccount>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion
    }
}