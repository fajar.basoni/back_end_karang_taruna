﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebServiceAPI.Class.DataAccessLayers;
using WebServiceAPI.Class.Generals;
using WebServiceAPI.Class.Parameters;
using WebServiceAPI.Class.Result;
using WebServiceAPI.Models;

namespace WebServiceAPI.Controllers
{
    [EnableCors("SiteCorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class MembershipsController : ControllerBase
    {
        #region Variables
        public IConfiguration Configuration { get; }
        public MembershipsController(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        #endregion

        #region GetListMemberships
        [HttpPost]
        [Route("GetListMemberships")]
        public async Task<ResultGetListMemberships> GetListMemberships([FromBody]ParamGetListMemberships param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultGetListMemberships objResult = new ResultGetListMemberships();
            MetaData metaData = new MetaData();
            List<ResultMemberships> elements = new List<ResultMemberships>();
            bool isValid = false;
            #endregion
            
            if (isValid == false)
            {
                try
                {
                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALMemberships(Configuration).GetListMemberships(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<ResultMemberships>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion

        #region GetDetailMemberships
        [HttpPost]
        [Route("GetDetailMemberships")]
        public async Task<ResultGetDetailMemberships> GetDetailMemberships([FromBody]ParamGetDetailMemberships param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultGetDetailMemberships objResult = new ResultGetDetailMemberships();
            MetaData metaData = new MetaData();
            List<ResultMemberships> elements = new List<ResultMemberships>();
            bool isValid = false;
            #endregion

            if (!string.IsNullOrWhiteSpace(param.id))
            {
                isValid = true;
            }

            if (isValid)
            {
                try
                {
                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALMemberships(Configuration).GetDetailMemberships(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<ResultMemberships>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion

        #region InsertMemberships
        [HttpPost]
        [Route("InsertMemberships")]
        public async Task<ResultInsertMemberships> InsertMemberships([FromBody]ParamInsertMemberships param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultInsertMemberships objResult = new ResultInsertMemberships();
            MetaData metaData = new MetaData();
            List<ResultMemberships> elements = new List<ResultMemberships>();
            bool isValid = false;
            #endregion

            if (!string.IsNullOrWhiteSpace(param.name)
                && !string.IsNullOrWhiteSpace(param.education_degree)
                && !string.IsNullOrWhiteSpace(param.department_id)
                && !string.IsNullOrWhiteSpace(param.status)
                && !string.IsNullOrWhiteSpace(param.begining_periode)
                && !string.IsNullOrWhiteSpace(param.ending_periode)
                && !string.IsNullOrWhiteSpace(param.image_path)
                && !string.IsNullOrWhiteSpace(param.is_actived)
                && !string.IsNullOrWhiteSpace(param.is_deleted)
                && !string.IsNullOrWhiteSpace(param.created_date)
                && !string.IsNullOrWhiteSpace(param.created_by)
                && !string.IsNullOrWhiteSpace(param.updated_date)
                && !string.IsNullOrWhiteSpace(param.updated_by))
            {
                isValid = true;
            }

            if (isValid)
            {
                try
                {
                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALMemberships(Configuration).InsertMemberships(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<ResultMemberships>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion

        #region UpdateMemberships
        [HttpPost]
        [Route("UpdateMemberships")]
        public async Task<ResultUpdateMemberships> UpdateMemberships([FromBody]ParamUpdateMemberships param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultUpdateMemberships objResult = new ResultUpdateMemberships();
            MetaData metaData = new MetaData();
            List<ResultMemberships> elements = new List<ResultMemberships>();
            bool isValid = false;
            #endregion

            if (!string.IsNullOrWhiteSpace(param.membership_id)
                && !string.IsNullOrWhiteSpace(param.membership_number)
                && !string.IsNullOrWhiteSpace(param.name)
                && !string.IsNullOrWhiteSpace(param.education_degree)
                && !string.IsNullOrWhiteSpace(param.department_id)
                && !string.IsNullOrWhiteSpace(param.status)
                && !string.IsNullOrWhiteSpace(param.begining_periode)
                && !string.IsNullOrWhiteSpace(param.ending_periode)
                && !string.IsNullOrWhiteSpace(param.image_path)
                && !string.IsNullOrWhiteSpace(param.is_actived)
                && !string.IsNullOrWhiteSpace(param.is_deleted)
                && !string.IsNullOrWhiteSpace(param.created_date)
                && !string.IsNullOrWhiteSpace(param.created_by)
                && !string.IsNullOrWhiteSpace(param.updated_date)
                && !string.IsNullOrWhiteSpace(param.updated_by))
            {
                isValid = true;
            }

            if (isValid)
            {
                try
                {
                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALMemberships(Configuration).UpdateMemberships(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<ResultMemberships>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion

        #region DeleteMemberships
        [HttpPost]
        [Route("DeleteMemberships")]
        public async Task<ResultDeleteMemberships> DeleteMemberships([FromBody]ParamDeleteMemberships param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultDeleteMemberships objResult = new ResultDeleteMemberships();
            MetaData metaData = new MetaData();
            List<string> elements = new List<string>();
            bool isValid = false;
            #endregion

            if (!string.IsNullOrWhiteSpace(param.id))
            {
                isValid = true;
            }

            if (isValid)
            {
                try
                {
                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALMemberships(Configuration).DeleteMemberships(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<string>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion
    }
}