﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebServiceAPI.Class.DataAccessLayers;
using WebServiceAPI.Class.Generals;
using WebServiceAPI.Class.Parameters;
using WebServiceAPI.Class.Result;
using WebServiceAPI.Models;

namespace WebServiceAPI.Controllers
{
    [EnableCors("SiteCorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        #region Variables
        public IConfiguration Configuration { get; }
        public MessageController(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        #endregion

        #region GetListMessage
        [HttpPost]
        [Route("GetListMessage")]
        public async Task<ResultGetListMessage> GetListMessage([FromBody] ParamGetListMessage param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultGetListMessage objResult = new ResultGetListMessage();
            MetaData metaData = new MetaData();
            List<ResultMessage> elements = new List<ResultMessage>();
            bool isValid = false;
            #endregion

            if(!string.IsNullOrWhiteSpace(param.limit)
                && !string.IsNullOrWhiteSpace(param.offset))
            {
                isValid = true;
            }

            if (isValid)
            {
                try
                {
                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALMessage(Configuration).GetListMessage(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<ResultMessage>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion

        #region InsertMessage
        [HttpPost]
        [Route("InsertMessage")]
        public async Task<ResultInsertMessage> InsertMessage([FromBody] ParamInsertMessage param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultInsertMessage objResult = new ResultInsertMessage();
            MetaData metaData = new MetaData();
            List<ResultMessage> elements = new List<ResultMessage>();
            bool isValid = false;
            #endregion

            //1. Validation parameter
            if (!string.IsNullOrWhiteSpace(param.full_name)
                && !string.IsNullOrWhiteSpace(param.email)
                && !string.IsNullOrWhiteSpace(param.subject)
                && !string.IsNullOrWhiteSpace(param.message))
            {
                isValid = true;
            }

            if (isValid)
            {
                try
                {
                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALMessage(Configuration).InsertMessage(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<ResultMessage>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion


        #region UpdateReadedMessage
        [HttpPost]
        [Route("UpdateReadedMessage")]
        public async Task<ResultUpdateReadedMessage> UpdateReadedMessage([FromBody] ParamUpdateReadedMessage param)
        {
            //1.  Declaration variable / Instansiasi object
            #region "Declaration Variable"
            SQLResult objResultDataAccess = new SQLResult();
            ResultUpdateReadedMessage objResult = new ResultUpdateReadedMessage();
            MetaData metaData = new MetaData();
            List<ResultMessage> elements = new List<ResultMessage>();
            bool isValid = false;
            #endregion

            if (!string.IsNullOrWhiteSpace(param.message_id))
            {
                isValid = true;
            }

            if (isValid)
            {
                try
                {
                    //2. Acces FN/SP DB
                    objResultDataAccess = new DALMessage(Configuration).UpdateReadedMessage(param);
                    if (objResultDataAccess.ERRORCODE.Equals(SQLResult.NOERROR))
                    {
                        //#. success {code=200}!
                        elements = (List<ResultMessage>)objResultDataAccess.TAG;
                        metaData.code = Constanta.CONST_RES_CD_SUCCESS;
                        metaData.message = Constanta.CONST_RES_MESSAGE_SUCCESS;
                        metaData.row_count = objResultDataAccess.ROWCOUNT;
                    }
                    else
                    {
                        if (objResultDataAccess.ERRORCODE.Equals(SQLResult.ERROR_EXCEPTION))
                        {
                            //#. error DB {code=500}!
                            metaData.code = Constanta.CONST_RES_CD_CATCH;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                        else
                        {
                            //#. error DB {code=600}!
                            metaData.code = objResultDataAccess.ERRORCODE;
                            metaData.message = objResultDataAccess.ERRORDESC;
                            metaData.row_count = objResultDataAccess.ROWCOUNT;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //#. error exception {code=500}!
                    metaData.code = Constanta.CONST_RES_CD_CATCH;
                    metaData.message = "Catch#1 " + ex.Message;
                }
            }
            else
            {
                //#. error DB {code=500}!
                metaData.code = Constanta.CONST_RES_CD_ERROR;
                metaData.message = Constanta.CONST_RES_MESSAGE_INVALIDINPUT;
            }

            //3. set value to object result
            objResult.meta_data = metaData;
            objResult.datas = elements;

            //4. return value
            return objResult;
        }
        #endregion
    }
}