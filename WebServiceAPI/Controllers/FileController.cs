﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebServiceAPI.Class.Parameters;

namespace WebServiceAPI.Controllers
{
    [EnableCors("SiteCorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        [HttpPost]
        [Route("UploadSingle")]
        public ActionResult UploadFile([FromBody] ParamFileUpload param)
        {
            try
            {
                if (param.base64.Length > 0)
                {
                    string fileName = Guid.NewGuid().ToString("N") + ".jpg";
                    var file_path = Path.Combine("Apps", "Files", fileName);
                    var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), file_path);

                    byte[] imageBytes = Convert.FromBase64String(param.base64);

                    System.IO.File.WriteAllBytes(pathToSave, imageBytes);

                    return Ok(new { file_path });
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, message);
            }

        }
    }
}