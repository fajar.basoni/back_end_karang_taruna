﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebServiceAPI.Models;

namespace WebServiceAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(o => o.InputFormatters.Insert(0, new RawRequestBodyFormatter()));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            var oCorsPolicyBuilder = new CorsPolicyBuilder();
            oCorsPolicyBuilder.AllowAnyHeader();
            oCorsPolicyBuilder.AllowAnyMethod();
            oCorsPolicyBuilder.AllowAnyOrigin();
            oCorsPolicyBuilder.AllowCredentials();

            services.AddCors(options =>
            {
                options.AddPolicy("SiteCorsPolicy", oCorsPolicyBuilder.Build());
            });

            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info {
                    Version = "v1",
                    Title = "Karang Taruna Desa Bojong Nangka Web Service",
                    Description = "Endpoint to access some general function of External Service",
                    Contact = new Swashbuckle.AspNetCore.Swagger.Contact
                    {
                        Name = "Karang Taruna Desa Bojong Nangka. Dept. Teknologi Informasi",
                        Email = "kartar.bojongnangka@gmail.com",
                        Url = string.Empty,
                    },
                    License = new Swashbuckle.AspNetCore.Swagger.License
                    {
                        Name = "digunakan untuk keperluan website karang taruna desa bojong nangka",
                    }
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Web Service Karang Taruna Desa Bojong Nangka V1");
                c.RoutePrefix = string.Empty;
            });


            app.UseHttpsRedirection();
            app.UseMvc();

            // ********************
            // USE CORS - might not be required.
            // ********************
            app.UseCors("SiteCorsPolicy");
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"Apps")),
                RequestPath = new PathString("/Apps")
            });
            
        }
    }
}
